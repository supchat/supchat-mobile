import 'package:mockito/mockito.dart' as _i1;
import 'package:supchat/user/data/customer_user_repository.dart' as _i2;
import 'package:logger/src/logger.dart' as _i3;
import 'package:supchat/billing/data/billing_repository.dart' as _i4;

/// A class which mocks [CustomerUserRepository].
///
/// See the documentation for Mockito's code generation for more information.
class MockCustomerUserRepository extends _i1.Mock
    implements _i2.CustomerUserRepository {
  MockCustomerUserRepository() {
    _i1.throwOnMissingStub(this);
  }
}

/// A class which mocks [Logger].
///
/// See the documentation for Mockito's code generation for more information.
class MockLogger extends _i1.Mock implements _i3.Logger {
  MockLogger() {
    _i1.throwOnMissingStub(this);
  }
}

/// A class which mocks [BillingRepository].
///
/// See the documentation for Mockito's code generation for more information.
class MockBillingRepository extends _i1.Mock implements _i4.BillingRepository {
  MockBillingRepository() {
    _i1.throwOnMissingStub(this);
  }
}
