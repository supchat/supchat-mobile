import 'package:logger/logger.dart';
import 'package:mockito/annotations.dart';
import 'package:supchat/billing/data/billing_repository.dart';
import 'package:supchat/profile/bloc/settings_cubit.dart';
import 'package:supchat/user/data/customer_user_repository.dart';
import 'package:test/test.dart';

import 'settings_cubit_test.mocks.dart';

@GenerateMocks([CustomerUserRepository, Logger, BillingRepository])
void main() {
  group("SettingCubit", () {
    SettingsCubit settingsCubit;

    setUp(() {
      settingsCubit = SettingsCubit(
        MockCustomerUserRepository(),
        MockLogger(),
        MockBillingRepository(),
      );
    });

    tearDown(() {
      settingsCubit.close();
    });

    test("Changing username", () {
      settingsCubit.loadUser();
    });
  });
}
