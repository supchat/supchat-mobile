import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart' as auth;
import 'package:firebase_storage/firebase_storage.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:supchat/utils/connectivity_manager.dart';
import 'package:supchat/utils/offline_exception.dart';
import 'package:supchat_chat/chat/data/user_repository.dart';
import 'package:supchat_chat/chat/model/chat_user.dart';
import 'package:supchat_chat/chat_config.dart';
import 'package:path/path.dart';

import '../model/user.dart';

class CustomerUserRepository extends UserRepository {
  final ChatConfig _config;
  final ConnectivityManager connectivityManager;

  CustomerUserRepository(this._config, this.connectivityManager);

  Future<User> getUser() async {
    final prefs = await SharedPreferences.getInstance();
    final id = prefs.getString('id');
    if (id == null) {
      return User.empty;
    }
    final nick = prefs.getString('nickname');
    final pushToken = prefs.getString('pushToken');
    final photoUrl = prefs.getString('photoUrl');
    return User(
      id: id,
      nickname: nick,
      photoUrl: photoUrl,
      pushToken: pushToken,
    );
  }

  Future<dynamic> createIfNotExists(User user) async {
    final userCollectionPath = 'customers';
    final documentPath = '$userCollectionPath/${user.id}';
    FirebaseFirestore firestore = FirebaseFirestore.instance;
    var doc = firestore.doc(documentPath);
    final remoteUserData = await doc.get();
    if (!remoteUserData.exists) {
      await _saveUserLocally(user);
      final map = user.toMap();
      map['createdAt'] = FieldValue.serverTimestamp();
      return doc.set(map);
    } else {
      final existingUser = user.copyWith(
        nickname: remoteUserData['nickname'],
        photoUrl: remoteUserData['photoUrl'],
      );
      await _saveUserLocally(existingUser);
    }
  }

  Future<void> update(User user) async {
    final isOnline = await connectivityManager.isConnected();
    if (!isOnline) {
      throw OfflineException("No Internet connection");
    }

    final userCollectionPath = 'customers';
    final documentPath = '$userCollectionPath/${user.id}';
    FirebaseFirestore firestore = FirebaseFirestore.instance;
    await firestore
        .doc(documentPath)
        .set(user.toMap(), SetOptions(merge: true));
    await _saveUserLocally(user);
  }

  _saveUserLocally(User user) async {
    final prefs = await SharedPreferences.getInstance();
    await prefs.setString('id', user.id);
    await prefs.setString('nickname', user.nickname);
    await prefs.setString('photoUrl', user.photoUrl);
    await prefs.setString('pushToken', user.pushToken);
  }

  _deleteLocalUser() async {
    final prefs = await SharedPreferences.getInstance();
    await prefs.setString('id', null);
    await prefs.setString('nickname', null);
    await prefs.setString('photoUrl', null);
    await prefs.setString('pushToken', null);
  }

  @override
  Future<ChatUser> getChatUser() async {
    final localUser = await getUser();
    if (localUser == User.empty) {
      return ChatUser.empty;
    }
    return ChatUser(localUser.id, localUser.timestamp, localUser.nickname,
        localUser.photoUrl, localUser.pushToken);
  }

  Future<dynamic> updatePushToken(String token) async {
    final prefs = await SharedPreferences.getInstance();
    await prefs.setString('pushToken', token);
  }

  Future<dynamic> uploadNewPhoto(String photoUrl, String customerId) async {
    final isOnline = await connectivityManager.isConnected();
    if (!isOnline) {
      throw OfflineException("No Internet connection");
    }

    final file = File(photoUrl);
    final fileName = basename(file.path);
    final reference =
        FirebaseStorage.instance.ref('$customerId/profilePicture/$fileName');

    final previousPhotos = await FirebaseStorage.instance
        .ref('$customerId/profilePicture')
        .listAll();
    final result = await reference.putFile(file);
    previousPhotos.items.forEach((element) async {
      await element.delete();
    });

    final newPhotoUrl = await result.ref.getDownloadURL();
    return newPhotoUrl;
  }

  Future<void> logOut() async {
    await auth.FirebaseAuth.instance.signOut();
    await _deleteLocalUser();
  }
}
