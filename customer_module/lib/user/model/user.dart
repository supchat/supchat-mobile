class User {
  final String id;
  final int timestamp;
  final String nickname;
  final String photoUrl;
  final String pushToken;

  User(
      {this.id,
      this.timestamp,
      this.nickname,
      this.photoUrl,
      this.pushToken});

  static User empty = User(
      id: null,
      timestamp: null,
      nickname: null,
      photoUrl: null,
      pushToken: null);

  Map<String, dynamic> toMap() {
    return {
      'id': this.id,
      'timestamp': this.timestamp,
      'nickname': this.nickname,
      'photoUrl': this.photoUrl,
      'pushToken': this.pushToken,
    };
  }

  factory User.fromMap(Map<String, dynamic> map) {
    return new User(
      id: map['id'] as String,
      timestamp: map['timestamp'] as int,
      nickname: map['nickname'] as String,
      photoUrl: map['photoUrl'] as String,
      pushToken: map['pushToken'] as String,
    );
  }

  User copyWith({
    String id,
    int timestamp,
    String nickname,
    String photoUrl,
    String pushToken,
  }) {
    if ((id == null || identical(id, this.id)) &&
        (timestamp == null || identical(timestamp, this.timestamp)) &&
        (nickname == null || identical(nickname, this.nickname)) &&
        (photoUrl == null || identical(photoUrl, this.photoUrl)) &&
        (pushToken == null || identical(pushToken, this.pushToken))) {
      return this;
    }

    return new User(
      id: id ?? this.id,
      timestamp: timestamp ?? this.timestamp,
      nickname: nickname ?? this.nickname,
      photoUrl: photoUrl ?? this.photoUrl,
      pushToken: pushToken ?? this.pushToken,
    );
  }

  @override
  String toString() {
    return 'User{id: $id, timestamp: $timestamp, nickname: $nickname, photoUrl: $photoUrl, pushToken: $pushToken}';
  }
}
