import 'package:bloc/bloc.dart';
import 'package:flutter/services.dart';
import 'package:logger/logger.dart';
import 'package:meta/meta.dart';
import 'package:purchases_flutter/object_wrappers.dart';
import 'package:supchat/analytics/data/analytics_repository.dart';
import 'package:supchat/billing/data/billing_repository.dart';

part 'billing_state.dart';

class BillingCubit extends Cubit<BillingState> {
  final BillingRepository _billingRepository;
  final Logger _logger;
  final AnalyticsRepository _analyticsRepository;

  BillingCubit(this._billingRepository, this._logger, this._analyticsRepository) : super(BillingInitial());

  void loadPackages() async {
    _analyticsRepository.logEvent("billing_load_packages");
    try {
      await _billingRepository.setup();
      final offerings = await _billingRepository.getOfferings();
      emit(
          BillingPackages(
              [offerings.current.monthly, offerings.current.annual]));
    } catch (e) {
      _analyticsRepository.logEvent("billing_failed_setup");
      rethrow;
    }
  }

  void purchase(Package package) async {
    _analyticsRepository.logEvent("billing_purchase_click");

    try {
      final result = await _billingRepository.purchase(package);
      final membership = result.entitlements.all["membership"];
      final success = membership?.isActive == true;

      if (success) {
        _analyticsRepository.logEvent("billing_purchase_success");
        emit(BillingSuccess());
      } else {
        _analyticsRepository.logEvent("billing_purchase_fail");
        emit(BillingError("Purchase not successful"));
      }
    } on PlatformException catch (e) {
      if (e.code == "1") {
        _analyticsRepository.logEvent("billing_purchase_fail_cancelled");
        emit(BillingError("Purchase was cancelled"));
      }
      _logger.e(e.toString());
    } catch (e) {
      _analyticsRepository.logEvent("billing_purchase_fail");
      emit(BillingError("Purchase error ${e.toString()}"));
      _logger.e(e.toString());
    }
  }
}
