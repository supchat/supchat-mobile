part of 'billing_cubit.dart';

@immutable
abstract class BillingState {}

class BillingInitial extends BillingState {}

class BillingSuccess extends BillingState {}

class BillingError extends BillingState {
  final String error;

  BillingError(this.error);
}

class BillingPackages extends BillingState {
  final List<Package> packages;

  BillingPackages(this.packages);
}
