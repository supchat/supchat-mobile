import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:logger/logger.dart';
import 'package:supchat/analytics/data/analytics_repository.dart';
import 'package:supchat/billing/bloc/billing_cubit.dart';
import 'package:supchat/billing/data/billing_repository.dart';
import 'package:supchat/main/bloc/bloc.dart';
import 'package:supchat/profile/bloc/settings_cubit.dart';
import 'package:supchat/sl/getIt.dart';
import 'package:supchat/widgets/atoms/dialog_bg.dart';
import 'package:supchat/widgets/molecules/btn_filled.dart';
import 'package:supchat/widgets/molecules/btn_outlined.dart' as btn;
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:supchat/widgets/molecules/dialog_error.dart';
import 'package:supchat/widgets/molecules/dialog_success.dart';
import 'package:google_fonts/google_fonts.dart';

class BillingScreen extends StatelessWidget {
  final _cubit = BillingCubit(
      getIt<BillingRepository>(), getIt<Logger>(), getIt<AnalyticsRepository>())
    ..loadPackages();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: BlocConsumer<BillingCubit, BillingState>(
        cubit: _cubit,
        listener: (context, state) {
          if (state is BillingSuccess) {
            Navigator.of(context).pop(true);
          } else if (state is BillingError) {
            Navigator.of(context).pop(false);
            ErrorDialog.show(context, state.error);
          } else {}
        },
        buildWhen: (BillingState previous, BillingState state) =>
            state is BillingInitial || state is BillingPackages,
        builder: (context, state) => (state is BillingInitial)
            ? Center(child: CircularProgressIndicator())
            : Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Align(
                    alignment: AlignmentDirectional.centerEnd,
                    child: IconButton(
                      icon:
                          SvgPicture.asset("assets/icons/ic_dialog_close.svg"),
                      onPressed: () {
                        Navigator.pop(context);
                      },
                    ),
                  ),
                  Text("Subscribe to get a membership",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.montserrat(
                          textStyle: TextStyle(
                        fontSize: 22,
                        fontWeight: FontWeight.w500,
                      ))),
                  Padding(
                    padding: const EdgeInsets.all(24.0),
                    child: RichText(
                      textAlign: TextAlign.center,
                      text: TextSpan(
                          text:
                              "Unlock unlimited messages and unlimited chats. Unlimited integrations with apps and websites.",
                          style: GoogleFonts.montserrat(
                              textStyle:
                                  TextStyle(fontSize: 16, color: Colors.black)),
                          children: [
                            TextSpan(
                                text: " 1 week trial",
                                style: GoogleFonts.montserrat(
                                    textStyle:
                                        TextStyle(fontWeight: FontWeight.w500)))
                          ]),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 22.0),
                    child: FilledButton(
                      text:
                          '${(state as BillingPackages).packages[0].product.priceString} / Month',
                      onPressed: () {
                        _cubit.purchase((state as BillingPackages).packages[0]);
                      },
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 22.0),
                    child: btn.OutlinedButton(
                      text:
                          '${(state as BillingPackages).packages[1].product.priceString} / Year',
                      onPressed: () {
                        _cubit.purchase((state as BillingPackages).packages[1]);
                      },
                    ),
                  ),
                ],
              ),
      ),
    );
  }

  static Future<dynamic> showInDialog(BuildContext context) {
    return showDialog(
        barrierDismissible: true,
        barrierColor: Colors.white.withOpacity(0.4),
        context: context,
        builder: (context) => AlertDialog(
              elevation: 0.0,
              contentPadding: EdgeInsets.all(16),
              backgroundColor: Colors.transparent,
              insetPadding: EdgeInsets.all(10),
              content: DialogBg(
                padding: 0,
                child: Container(height: 410, child: BillingScreen()),
              ),
            ));
  }
}
