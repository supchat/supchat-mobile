import 'package:flutter/foundation.dart';
import 'package:purchases_flutter/purchases_flutter.dart';
import 'package:supchat/user/data/customer_user_repository.dart';

class BillingRepository {
  final CustomerUserRepository _customerUserRepository;

  var _isSetup = false;

  BillingRepository(this._customerUserRepository);

  Future<void> setup() async {
    if (_isSetup) return;
    if (!kReleaseMode) {
      await Purchases.setDebugLogsEnabled(true);
    }
    final user = await _customerUserRepository.getUser();
    await Purchases.setup("CjddvRwpzeidopxSlqQLNizaroEsNrHc",
        appUserId: user.id);
    _isSetup = true;
  }

  Future<Offerings> getOfferings() {
    return Purchases.getOfferings();
  }

  Future<PurchaserInfo> getPurchaserInfo() {
    return Purchases.getPurchaserInfo();
  }

  Future<PurchaserInfo> purchase(Package package) {
    return Purchases.purchasePackage(package);
  }
}
