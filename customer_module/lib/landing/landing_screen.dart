import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:supchat/auth/signin_screen.dart';
import 'package:supchat/auth/signup_screen.dart';
import 'package:supchat/widgets/app_styles.dart';
import 'package:supchat/widgets/molecules/btn_filled.dart';
import 'package:supchat/widgets/molecules/btn_outlined.dart' as btn;

class LandingScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Positioned.fill(
              child: SvgPicture.asset(
            'assets/landing_bg.svg',
            fit: BoxFit.fill,
          )),
          SafeArea(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Column(
                  children: [
                    Container(
                        alignment: AlignmentDirectional.topCenter,
                        margin: EdgeInsets.only(top: 55),
                        child: Text('Hi!',
                            style: GoogleFonts.montserrat(
                                textStyle: TextStyles.authTitle))),
                    Container(
                        height: 240,
                        alignment: AlignmentDirectional.topCenter,
                        margin: EdgeInsets.only(left: 40),
                        child: Image.asset('assets/images/landing_people.png')),
                    const SizedBox(height: 8.0),
                    Container(
                        alignment: AlignmentDirectional.topCenter,
                        margin: EdgeInsets.symmetric(horizontal: 55),
                        child: Text('Welcome to Supchat',
                            textAlign: TextAlign.center,
                            style: GoogleFonts.montserrat(
                                textStyle: TextStyles.authTitle))),
                  ],
                ),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        margin:
                            EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                        child: FilledButton(
                          text: 'Sign up',
                          onPressed: () => Navigator.of(context).push(
                              MaterialPageRoute(
                                  builder: (BuildContext context) =>
                                      SignUpScreen())),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(left: 20, right: 20),
                        child: btn.OutlinedButton(
                          text: 'Sign in',
                          onPressed: () => Navigator.of(context).push(
                              MaterialPageRoute(
                                  builder: (BuildContext context) =>
                                      LoginScreen())),
                        ),
                      )
                    ],
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
