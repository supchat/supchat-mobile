import 'package:flutter/gestures.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:logger/logger.dart';
import 'package:supchat/analytics/data/analytics_repository.dart';
import 'package:supchat/auth/auth_social_widget.dart';
import 'package:supchat/chats/chats_screen.dart';
import 'package:supchat/sl/getIt.dart';
import 'package:supchat/widgets/app_styles.dart';
import 'package:supchat/widgets/atoms/content_container.dart';
import 'package:supchat/widgets/atoms/txt_field_bg.dart';
import 'package:supchat/widgets/molecules/btn_filled.dart';
import 'package:supchat/widgets/molecules/dialog_error.dart';
import 'package:supchat/widgets/molecules/dialog_progress.dart';
import 'package:supchat/widgets/molecules/dialog_success.dart';
import 'package:supchat/widgets/molecules/input_text.dart';
import 'package:bloc/bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'bloc/auth_bloc.dart';
import 'bloc/auth_state.dart';
import 'data/auth_repository.dart';
import 'package:supchat/auth/data/social/facebook.dart';
import 'package:supchat/auth/data/social/google.dart';

import 'bloc/auth_bloc.dart';
import 'data/social/apple.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final _emailController = TextEditingController();

  final _passwordController = TextEditingController();

  @override
  void dispose() {
    _emailController.dispose();
    _passwordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Stack(
            alignment: AlignmentDirectional.center,
            children: [
              Container(
                  width: double.maxFinite,
                  height: 220.0,
                  color: Color(0xff8E91F4),
                  child: SvgPicture.asset('assets/login_bg.svg')),
              Text('Sign in',
                  textAlign: TextAlign.center,
                  style: GoogleFonts.montserrat(
                      textStyle:
                          TextStyles.authTitle.copyWith(color: Colors.white)))
            ],
          ),
          BlocProvider<AuthBloc>(
            create: (BuildContext context) => AuthBloc(
                getIt<AuthRepository>(),
                getIt<GoogleRepository>(),
                getIt<FacebookRepository>(),
                getIt<AppleRepository>(),
                getIt<AnalyticsRepository>()),
            child: Container(
              width: double.maxFinite,
              height: double.maxFinite,
              margin: const EdgeInsets.only(top: 190.0),
              child: BlocConsumer<AuthBloc, AuthState>(
                listener: (BuildContext context, AuthState state) {
                  if (state is ProgressState) {
                    ProgressDialog.show(context);
                  } else if (!(state is InitialAuthState)) {
                    Navigator.pop(context); //assume progress dialog is showing
                    if (state is ErrorState) {
                      ErrorDialog.show(context, state.text);
                    } else if (state is SuccessState) {
                      debugPrint("Success dialog show");
                      SuccessDialog.show(context, "Login is successful", () {
                        getIt<Logger>().i("Success dialog timeout");
                        Navigator.pushAndRemoveUntil(
                            context,
                            MaterialPageRoute(
                                builder: (context) => ChatsScreen(
                                      currentUserId: state.user.id,
                                    )),
                            (route) => true);
                      });
                    }
                  }
                },
                buildWhen: (AuthState previous, AuthState current) =>
                    current is InitialAuthState,
                builder: (BuildContext context, AuthState state) =>
                    ContentContainer(
                  child: SingleChildScrollView(
                    reverse: true,
                    child: Padding(
                      padding: const EdgeInsets.all(16.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          const SizedBox(height: 40.0),
                          InputText(
                            title: 'Email',
                            hint: 'name@sample.com',
                            controller: _emailController,
                          ),
                          InputText(
                            title: 'Password',
                            hint: '',
                            controller: _passwordController,
                            isPassword: true,
                          ),
                          const SizedBox(height: 77.0),
                          FilledButton(
                            text: 'Sign in',
                            onPressed: () {
                              BlocProvider.of<AuthBloc>(context).login(
                                  _emailController.text,
                                  _passwordController.text);
                            },
                          ),
                          const SizedBox(height: 8.0),
                          Builder(
                              builder: (BuildContext context) => SocialWidget(
                                    bloc: BlocProvider.of<AuthBloc>(context),
                                  )),
                          const SizedBox(height: 24.0),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
