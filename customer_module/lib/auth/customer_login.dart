// import 'dart:async';
// import 'dart:io';
//
// import 'package:cloud_firestore/cloud_firestore.dart';
// import 'package:fb_auth/data/blocs/auth/auth_bloc.dart';
// import 'package:fb_auth/data/blocs/auth/auth_event.dart';
// import 'package:fb_auth/data/blocs/auth/auth_state.dart';
// import 'package:fb_auth/data/classes/app.dart';
// import 'package:fb_auth/data/classes/auth_user.dart';
// import 'package:firebase_auth/firebase_auth.dart';
// import 'package:flutter/foundation.dart';
// import 'package:flutter/material.dart';
//
// //import 'package:google_sign_in/google_sign_in.dart';
// import 'package:shared_preferences/shared_preferences.dart';
//
// import '../const.dart';
// import '../chats/chats_screen.dart';
//
// import 'package:flutter_bloc/flutter_bloc.dart';
//
// import '../main/CustomerApp.dart';
// import '../sl/getIt.dart';
// import '../user/data/customer_user_repository.dart';
// import '../user/model/user.dart';
// import 'bloc/bloc.dart';
//
// class LoginScreen extends StatefulWidget {
//   LoginScreen({Key key, this.title}) : super(key: key);
//
//   final String title;
//
//   @override
//   CustomerLoginScreenState createState() => CustomerLoginScreenState();
// }
//
// class CustomerLoginScreenState extends State<LoginScreen> {
//   final FirebaseAuth firebaseAuth = FirebaseAuth.instance;
//
//   bool isLoading = false;
//   bool isLoggedIn = false;
//
//   String _email, _password, _name;
//   bool _createAccount = true;
//   final _formKey = GlobalKey<FormState>();
//
//   final _authBloc = AuthBloc(app: fbApp, saveUser: _saveUser);
//
//   static _saveUser(AuthUser user) async {
//     getIt<CustomerUserRepository>().createIfNotExists(User(
//         id: user.uid,
//         timestamp: DateTime.now().millisecondsSinceEpoch,
//         nickname: user.displayName,
//         photoUrl: user.photoUrl));
//   }
//
//   @override
//   void initState() {
//     super.initState();
//     _authBloc.listen((authState) {
//       if (authState is AuthLoadingState) {
//         _setProgress(true);
//       }
//       if (authState is AuthErrorState) {
//         _setProgress(false);
//       }
//       if (authState is LoggedInState) {
//         _setProgress(false);
//         moveToChat(authState.user.uid);
//       }
//     });
//   }
//
//   @override
//   void dispose() {
//     _authBloc.close();
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return BlocProvider<LocalAuthBloc>(
//       create: (context) => LocalAuthBloc()..add(CheckLogin()),
//       child: BlocListener<LocalAuthBloc, LocalAuthState>(
//         condition: (prevState, state) => state is LoggedInAuthState,
//         listener: (BuildContext context, state) {
//           final authState = state as LoggedInAuthState;
//           Navigator.push(
//             context,
//             MaterialPageRoute(
//                 builder: (context) =>
//                     ChatsScreen(currentUserId: authState.userId)),
//           );
//         },
//         child: Scaffold(
//             appBar: AppBar(
//               title: Text(
//                 widget.title,
//                 style:
//                     TextStyle(color: primaryColor, fontWeight: FontWeight.bold),
//               ),
//               centerTitle: true,
//             ),
//             body: BlocBuilder<LocalAuthBloc, LocalAuthState>(
//               condition: (prevState, state) => state is LoggedOutAuthState,
//               builder: (BuildContext context, state) {
//                 return IndexedStack(
//                   index: (state is InitialLocalAuthState) ? 1 : 0,
//                   children: <Widget>[
//                     SingleChildScrollView(
//                       child: Form(
//                           key: _formKey,
//                           child: Column(
//                             children: <Widget>[
//                               Visibility(
//                                 visible: _createAccount,
//                                 child: ListTile(
//                                   title: TextFormField(
//                                     decoration: InputDecoration(
//                                         labelText: 'Display Name'),
//                                     onSaved: (val) => _name = val.trim(),
//                                   ),
//                                 ),
//                               ),
//                               ListTile(
//                                 title: TextFormField(
//                                   decoration:
//                                       InputDecoration(labelText: 'Email'),
//                                   keyboardType: TextInputType.emailAddress,
//                                   validator: (val) =>
//                                       val.isEmpty ? 'Email Required' : null,
//                                   onSaved: (val) => _email = val.trim(),
//                                 ),
//                               ),
//                               ListTile(
//                                 title: TextFormField(
//                                   decoration:
//                                       InputDecoration(labelText: 'Password'),
//                                   validator: (val) =>
//                                       val.isEmpty ? 'Password Required' : null,
//                                   onSaved: (val) => _password = val.trim(),
//                                 ),
//                               ),
//                               if (_createAccount)
//                                 ListTile(
//                                     title: RaisedButton(
//                                   child: Text('Sign Up'),
//                                   onPressed: () {
//                                     if (_formKey.currentState.validate()) {
//                                       _formKey.currentState.save();
//                                       _signUp(_email, _password, _name);
//                                     }
//                                   },
//                                 ))
//                               else
//                                 ListTile(
//                                     title: RaisedButton(
//                                   child: Text('Login'),
//                                   onPressed: () {
//                                     if (_formKey.currentState.validate()) {
//                                       _formKey.currentState.save();
//                                       _login(_email, _password);
//                                     }
//                                   },
//                                 )),
//
//                               ListTile(
//                                   title: FlatButton(
//                                 child: Text(_createAccount
//                                     ? 'Already have an account?'
//                                     : 'Create a new account?'),
//                                 onPressed: () {
//                                   if (mounted)
//                                     setState(() {
//                                       _createAccount = !_createAccount;
//                                     });
//                                 },
//                               )),
// //                      if (state is AuthLoadingState) CircularProgressIndicator(),
//                             ],
//                           )),
//                     ),
//                     // Loading
//                     Positioned(
//                         child: Container(
//                       child: Center(
//                         child: CircularProgressIndicator(
//                           valueColor: AlwaysStoppedAnimation<Color>(themeColor),
//                         ),
//                       ),
//                       color: Colors.white.withOpacity(0.8),
//                     )),
//                   ],
//                 );
//               },
//             )),
//       ),
//     );
//   }
//
//   _signUp(String email, String password, String name) async {
//     _authBloc.add(CreateAccount(email, password, displayName: name));
//   }
//
//   _login(String email, String password) async {
//     _authBloc.add(LoginEvent(email, password));
//   }
//
//   Future moveToChat(String userId) async {
//     Navigator.push(
//       context,
//       MaterialPageRoute(
//           builder: (context) => ChatsScreen(currentUserId: userId)),
//     );
//   }
//
//   _setProgress(bool isProgress) {
//     setState(() {
//       isLoading = isProgress;
//     });
//   }
// }
