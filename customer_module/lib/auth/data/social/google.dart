import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:supchat/user/data/customer_user_repository.dart';
import 'package:supchat/user/model/user.dart' as localUser;

class GoogleRepository {
  final CustomerUserRepository _userRepository;

  GoogleRepository(this._userRepository);

  Future<localUser.User> signIn() async {
    // Trigger the authentication flow
    final GoogleSignInAccount googleUser = await GoogleSignIn().signIn();

    if (googleUser == null) return null;

    // Obtain the auth details from the request
    final GoogleSignInAuthentication googleAuth =
        await googleUser.authentication;

    // Create a new credential
    final GoogleAuthCredential credential = GoogleAuthProvider.credential(
      accessToken: googleAuth.accessToken,
      idToken: googleAuth.idToken,
    );

    // Once signed in, return the UserCredential
    final creds = await FirebaseAuth.instance.signInWithCredential(credential);
    var user = localUser.User(
      id: creds.user.uid,
      nickname: creds.additionalUserInfo.profile['name'],
      photoUrl: creds.additionalUserInfo.profile['picture'],
    );
    _userRepository.createIfNotExists(user);
    return user;
  }
}
