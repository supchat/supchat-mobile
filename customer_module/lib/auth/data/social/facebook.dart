import 'package:firebase_auth/firebase_auth.dart';
import 'package:supchat/user/data/customer_user_repository.dart';
import 'package:supchat/user/model/user.dart' as localUser;
import 'package:flutter_facebook_auth/flutter_facebook_auth.dart';

class FacebookRepository {
  final CustomerUserRepository _userRepository;

  FacebookRepository(this._userRepository);

  Future<localUser.User> signIn() async {
    // Trigger the sign-in flow
    final AccessToken result = await FacebookAuth.instance.login();

    // Create a credential from the access token
    final FacebookAuthCredential facebookAuthCredential =
        FacebookAuthProvider.credential(result.token);

    // Once signed in, return the UserCredential
    final userCredential = await FirebaseAuth.instance
        .signInWithCredential(facebookAuthCredential);

    var user = localUser.User(
        id: userCredential.user.uid,
        nickname: userCredential.user.displayName,
        photoUrl: userCredential.user.photoURL
    );
    _userRepository.createIfNotExists(user);
    return user;
  }
}
