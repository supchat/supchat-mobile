import 'package:firebase_auth/firebase_auth.dart' as fb;
import 'package:flutter/cupertino.dart';
import 'package:logger/logger.dart';
import 'package:supchat/sl/getIt.dart';
import 'package:supchat/user/data/customer_user_repository.dart';
import 'package:supchat/user/model/user.dart';

class AuthRepository {
  final CustomerUserRepository _userRepository;

  final _auth = fb.FirebaseAuth.instance;

  AuthRepository(this._userRepository);

  Future<User> register(String email, String password) async {
    try {
      final fbUser = await _auth.createUserWithEmailAndPassword(
          email: email, password: password);
      var user = User(id: fbUser.user.uid, nickname: fbUser.user.displayName);
      _userRepository.createIfNotExists(user);
      return user;
    } on fb.FirebaseAuthException catch (e) {
      if (e.code == 'weak-password') {
        throw AuthError.weekPassword;
      } else if (e.code == 'email-already-in-use') {
        throw AuthError.emailAlreadyUsed;
      } else {
        getIt<Logger>().e("Error on auth: ${e.code}");
        throw AuthError.somethingWentWrong;
      }
    } catch (e) {
      throw AuthError.somethingWentWrong;
    }
  }

  Future<User> login(String email, String password) async {
    try {
      final fbUser = await _auth.signInWithEmailAndPassword(
          email: email, password: password);
      var user = User(id: fbUser.user.uid, nickname: fbUser.user.displayName);
      _userRepository.createIfNotExists(user);
      return user;
    } on fb.FirebaseAuthException catch (e) {
      if (e.code == 'user-not-found') {
        throw AuthError.userNotFound;
      } else if (e.code == 'wrong-password') {
        throw AuthError.wrongPassword;
      } else {
        getIt<Logger>().e("Error on auth: ${e.code}");
        throw AuthError.somethingWentWrong;
      }
    } catch (e) {
      throw AuthError.somethingWentWrong;
    }
  }
}

enum AuthError {
  weekPassword,
  emailAlreadyUsed,
  userNotFound,
  wrongPassword,
  somethingWentWrong,
  emptyEmail,
  emptyPassword
}
