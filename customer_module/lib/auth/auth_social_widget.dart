import 'dart:io';

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:supchat/auth/bloc/auth_bloc.dart';
import 'package:supchat/widgets/molecules/dialog_progress.dart';
import 'package:supchat/widgets/molecules/dialog_success.dart';

class SocialWidget extends StatelessWidget {
  final AuthBloc bloc;

  const SocialWidget({Key key, this.bloc}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
          'Or use',
          style: GoogleFonts.montserrat(
              textStyle: TextStyle(
                  fontWeight: FontWeight.w400,
                  fontSize: 16,
                  color: Color(0xffD1D5D6))),
        ),
        if (Platform.isIOS)
          IconButton(
            padding: EdgeInsets.zero,
            icon: SvgPicture.asset('assets/icons/ic_apple.svg'),
            onPressed: () {
              bloc.appleSignIn();
            },
          ),
        IconButton(
          padding: EdgeInsets.zero,
          icon: SvgPicture.asset(
            'assets/icons/ic_google.svg',
          ),
          onPressed: () {
            bloc.googleSignIn();
          },
        ),
        IconButton(
          padding: EdgeInsets.zero,
          icon: SvgPicture.asset('assets/icons/ic_fb.svg'),
          onPressed: () {
            bloc.facebookSignIn();
          },
        ),
      ],
    );
  }
}
