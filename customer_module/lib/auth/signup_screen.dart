import 'package:flutter/gestures.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:logger/logger.dart';
import 'package:supchat/analytics/data/analytics_repository.dart';
import 'package:supchat/auth/auth_social_widget.dart';
import 'package:supchat/auth/data/social/facebook.dart';
import 'package:supchat/auth/data/social/google.dart';
import 'package:supchat/auth/signin_screen.dart';
import 'package:supchat/chats/chats_screen.dart';
import 'package:supchat/sl/getIt.dart';
import 'package:supchat/widgets/app_styles.dart';
import 'package:supchat/widgets/atoms/content_container.dart';
import 'package:supchat/widgets/atoms/txt_field_bg.dart';
import 'package:supchat/widgets/molecules/btn_filled.dart';
import 'package:supchat/widgets/molecules/dialog_error.dart';
import 'package:supchat/widgets/molecules/dialog_progress.dart';
import 'package:supchat/widgets/molecules/dialog_success.dart';
import 'package:supchat/widgets/molecules/input_text.dart';
import 'package:bloc/bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'bloc/auth_bloc.dart';
import 'bloc/auth_state.dart';
import 'data/auth_repository.dart';
import 'data/social/apple.dart';

class SignUpScreen extends StatefulWidget {
  @override
  _SignUpScreenState createState() => _SignUpScreenState();
}

class _SignUpScreenState extends State<SignUpScreen> {
  final _emailController = TextEditingController();

  final _passwordController = TextEditingController();

  @override
  void dispose() {
    _emailController.dispose();
    _passwordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Stack(
            alignment: AlignmentDirectional.center,
            children: [
              Container(
                  width: double.maxFinite,
                  height: 220,
                  color: Color(0xffFF5F3C),
                  child: SvgPicture.asset('assets/signup_bg.svg')),
              Text('Sign up',
                  textAlign: TextAlign.center,
                  style: GoogleFonts.montserrat(
                      textStyle:
                          TextStyles.authTitle.copyWith(color: Colors.white)))
            ],
          ),
          BlocProvider<AuthBloc>(
            create: (BuildContext context) => AuthBloc(
                getIt<AuthRepository>(),
                getIt<GoogleRepository>(),
                getIt<FacebookRepository>(),
                getIt<AppleRepository>(),
                getIt<AnalyticsRepository>()),
            child: Container(
              width: double.maxFinite,
              height: double.maxFinite,
              margin: EdgeInsets.only(top: 190),
              child: BlocConsumer<AuthBloc, AuthState>(
                listener: (BuildContext context, AuthState state) {
                  if (state is ProgressState) {
                    ProgressDialog.show(context);
                  } else if (!(state is InitialAuthState)) {
                    Navigator.pop(context); //assume progress dialog is showing
                    if (state is ErrorState) {
                      ErrorDialog.show(context, state.text);
                    } else if (state is SuccessState) {
                      debugPrint("Success dialog show");
                      SuccessDialog.show(context, "Registration is successful",
                          () {
                        getIt<Logger>().i("Success dialog timeout");
                        Navigator.pushAndRemoveUntil(
                            context,
                            MaterialPageRoute(
                                builder: (context) => ChatsScreen(
                                      currentUserId: state.user.id,
                                    )),
                            (route) => true);
                      });
                    }
                  }
                },
                buildWhen: (AuthState previous, AuthState current) =>
                    current is InitialAuthState,
                builder: (BuildContext context, AuthState state) =>
                    ContentContainer(
                  child: SingleChildScrollView(
                    reverse: true,
                    child: Padding(
                      padding: const EdgeInsets.all(16.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          SizedBox(
                            height: 40,
                          ),
                          InputText(
                            title: 'Email',
                            hint: 'name@sample.com',
                            controller: _emailController,
                          ),
                          InputText(
                            title: 'Password',
                            hint: '',
                            controller: _passwordController,
                            isPassword: true,
                          ),
                          SizedBox(
                            height: 77,
                          ),
                          FilledButton(
                            text: 'Sign up',
                            onPressed: () {
                              BlocProvider.of<AuthBloc>(context).register(
                                  _emailController.text,
                                  _passwordController.text);
                            },
                          ),
                          SizedBox(
                            height: 8,
                          ),
                          Builder(
                              builder: (BuildContext context) => SocialWidget(
                                    bloc: BlocProvider.of<AuthBloc>(context),
                                  )),
                          SizedBox(
                            height: 24,
                          ),
                          GestureDetector(
                            onTap: () => Navigator.of(context).push(
                                MaterialPageRoute(
                                    builder: (BuildContext context) =>
                                        LoginScreen())),
                            child: RichText(
                              textAlign: TextAlign.center,
                              text: TextSpan(
                                  text: 'Already have an account? ',
                                  style: GoogleFonts.montserrat(
                                      textStyle: TextStyle(
                                    color: Color(0xffA8ABB0),
                                    fontSize: 14,
                                    letterSpacing: 0,
                                    fontWeight: FontWeight.normal,
                                  )),
                                  children: <TextSpan>[
                                    TextSpan(
                                      text: 'Log in',
                                      style: GoogleFonts.montserrat(
                                          textStyle: TextStyle(
                                        color: Color(0xff4176FF),
                                        fontSize: 14,
                                        letterSpacing: 0,
                                        fontWeight: FontWeight.normal,
                                      )),
                                    )
                                  ]),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
