import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:email_validator/email_validator.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:supchat/analytics/data/analytics_repository.dart';
import 'package:supchat/auth/data/auth_repository.dart';
import 'package:supchat/auth/data/social/apple.dart';
import 'package:supchat/auth/data/social/facebook.dart';
import 'package:supchat/auth/data/social/google.dart';
import 'package:supchat/user/model/user.dart';
import 'bloc.dart';

class AuthBloc extends Cubit<AuthState> {
  final AuthRepository _authRepository;
  final GoogleRepository _googleRepository;
  final FacebookRepository _facebookRepository;
  final AppleRepository _appleRepository;
  final AnalyticsRepository _analyticsRepository;

  AuthBloc(
      this._authRepository,
      this._googleRepository,
      this._facebookRepository,
      this._appleRepository,
      this._analyticsRepository)
      : super(InitialAuthState());

  login(String email, String password) async {
    await _analyticsRepository.logEvent("try_login_with_email_password");

    try {
      emit(ProgressState());
      if (!isValidEmail(email)) {
        emit(ErrorState("Invalid email", AuthError.emptyEmail));
      } else if (!isValidPassword(password)) {
        emit(ErrorState("Invalid password", AuthError.emptyPassword));
      } else {
        final user = await _authRepository.login(email, password);
        emit(SuccessState(user));
        initUser(user);
      }
    } on AuthError catch (e) {
      emit(ErrorState(e.toString(), e));
    } catch (e) {
      emit(ErrorState(e.toString(), AuthError.somethingWentWrong));
    }
  }

  register(String email, String password) async {
    await _analyticsRepository.logEvent("try_sign_up_with_email_password");

    try {
      emit(ProgressState());
      if (!isValidEmail(email)) {
        emit(ErrorState("Empty email", AuthError.emptyEmail));
      } else if (!isValidPassword(password)) {
        emit(ErrorState("Empty password", AuthError.emptyPassword));
      } else {
        final user = await _authRepository.register(email, password);
        emit(SuccessState(user));
        initUser(user);
      }
    } on AuthError catch (e) {
      emit(ErrorState(e.toString(), e));
    } catch (e) {
      emit(ErrorState(e.toString(), AuthError.somethingWentWrong));
    }
  }

  googleSignIn() async {
    await _analyticsRepository.logEvent("try_google_sign_in");
    await _signIn(() => _googleRepository.signIn());
  }

  facebookSignIn() async {
    await _analyticsRepository.logEvent("try_fb_sign_in");
    await _signIn(() => _facebookRepository.signIn());
  }

  appleSignIn() async {
    await _analyticsRepository.logEvent("try_apple_sign_in");
    await _signIn(() => _appleRepository.signIn());
  }

  _signIn(Function signInMethod) async {
    try {
      emit(ProgressState());
      final user = await signInMethod();
      if (user != null) {
        emit(SuccessState(user));
        initUser(user);
      } else
        emit(HiddenProgressState());
    } catch (e) {
      emit(ErrorState(e.toString(), AuthError.somethingWentWrong));
    }
  }

  bool isValidPassword(String password) {
    return password?.isNotEmpty == true;
  }

  bool isValidEmail(String email) {
    return email?.isNotEmpty == true && EmailValidator.validate(email);
  }

  void initUser(User user) async {
    await FirebaseCrashlytics.instance.setUserIdentifier(user.id);
    await _analyticsRepository.setUserId(user.id);
    await _analyticsRepository.logEvent("successful_sign_in");
    //subscribe to push notifications
    //load chats
    //
  }
}
