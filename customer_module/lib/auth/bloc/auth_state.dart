import 'package:meta/meta.dart';
import 'package:supchat/auth/bloc/bloc.dart';
import 'package:supchat/auth/data/auth_repository.dart';
import 'package:supchat/user/model/user.dart';

@immutable
abstract class AuthState {}

class InitialAuthState extends AuthState {}

class ProgressState extends AuthState {}

class HiddenProgressState extends AuthState {}

class SuccessState extends AuthState {
  final User user;

  SuccessState(this.user);
}

class ErrorState extends AuthState {
  final AuthError error;
  final String text;

  ErrorState(this.text, this.error);

  @override
  String toString() {
    return "ErrorState: $text";
  }
}
