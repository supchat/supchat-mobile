import 'package:flutter/foundation.dart';

class Details {
  final String clientId;
  final List<DetailsEntry> entries;
  final DetailsEntry name;

  Details(this.clientId, this.entries, this.name);
}

class DetailsEntry {
  final String id;
  final String type;
  final String name;
  final String value;

//<editor-fold desc="Data Methods" defaultstate="collapsed">

  const DetailsEntry({
    @required this.id,
    @required this.type,
    @required this.name,
    @required this.value,
  });

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is DetailsEntry &&
          runtimeType == other.runtimeType &&
          id == other.id &&
          type == other.type &&
          name == other.name &&
          value == other.value);

  @override
  int get hashCode =>
      id.hashCode ^ type.hashCode ^ name.hashCode ^ value.hashCode;

  @override
  String toString() {
    return 'DetailsEntry{' +
        ' id: $id,' +
        ' type: $type,' +
        ' name: $name,' +
        ' value: $value,' +
        '}';
  }

  DetailsEntry copyWith({
    String id,
    String type,
    String name,
    String value,
  }) {
    return new DetailsEntry(
      id: id ?? this.id,
      type: type ?? this.type,
      name: name ?? this.name,
      value: value ?? this.value,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'id': this.id,
      'type': this.type,
      'name': this.name,
      'value': this.value,
    };
  }

  factory DetailsEntry.fromMap(Map<String, dynamic> map) {
    return new DetailsEntry(
      id: map['id'] as String,
      type: map['type'] as String,
      name: map['name'] as String,
      value: map['value'] as String,
    );
  }

//</editor-fold>
}
