import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import '../details/data/details_repository.dart';

import 'bloc/bloc.dart';
import 'model/Details.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class DetailsScreen extends StatelessWidget {
  final String userId;
  final String customerId;

  DetailsBloc bloc;

  DetailsScreen({Key key, this.userId, this.customerId}) : super(key: key) {
    bloc = DetailsBloc(DetailsRepository(userId, customerId))
      ..add(DetailsOpenedEvent(userId));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: BlocBuilder<DetailsBloc, DetailsState>(
          cubit: bloc,
          builder: (BuildContext context, DetailsState state) => (state
                  is DetailsLoadedState)
              ? Column(
                  children: <Widget>[
                    buildTopBar(context, bloc),
                    buildContentSection(
                      context,
                      bloc,
                      state.details.name,
                      state.details.entries,
                    )
                  ],
                )
              : Container(child: Center(child: CircularProgressIndicator())),
        ),
      ),
    );
  }

  Padding buildContentSection(BuildContext context, DetailsBloc detailsBloc,
      DetailsEntry nameEntry, List<DetailsEntry> entries) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 12.0),
      child: SingleChildScrollView(
        padding: EdgeInsets.symmetric(vertical: 10.0),
        child: Column(
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            buildNameSection(nameEntry),
            ...entries
                .map((entry) => buildEntryRow(context, detailsBloc, entry))
          ],
        ),
      ),
    );
  }

  Widget buildEntryRow(BuildContext context, DetailsBloc detailsBloc,
      DetailsEntry detailsEntry) {
    return buildSimpleItem(context, detailsBloc, detailsEntry);
  }

  Widget buildSimpleItem(BuildContext context, DetailsBloc detailsBloc,
      DetailsEntry detailsEntry) {
    return GestureDetector(
      onTap: () {
        if (detailsEntry.type == 'custom')
          showEntryDialog(context, detailsBloc, detailsEntry: detailsEntry);
      },
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 6.0),
        child: Row(
          children: <Widget>[
            Icon(
              getEntryIcon(detailsEntry.type),
              color: Colors.grey,
              size: 20,
            ),
            Visibility(
                visible: detailsEntry.name != null,
                child: SizedBox(
                  width: 4.0,
                )),
            Visibility(
              visible: detailsEntry.name != null,
              child: Text(detailsEntry.name ?? "",
                  style: GoogleFonts.montserrat(
                      textStyle: TextStyle(
                          fontSize: 14,
                          color: Color(0xff0F2253),
                          fontWeight: FontWeight.w400))),
            ),
            SizedBox(
              width: 4.0,
            ),
            Text(detailsEntry.value,
                style: GoogleFonts.montserrat(
                    textStyle: TextStyle(
                        fontSize: 14,
                        color: Color(0xff0F2253),
                        fontWeight: FontWeight.w500))),
          ],
        ),
      ),
    );
  }

  IconData getEntryIcon(String type) {
    switch (type) {
      case "name":
        return Icons.account_circle;
      case "location":
        return Icons.location_on;
      case "email":
        return Icons.alternate_email;
      case "platform":
        return Icons.computer;
      case "phone":
        return Icons.phone;
      case "user_id":
        return Icons.local_library;
      case "first_seen":
      case "last_seen":
        return Icons.calendar_today;
      default:
        return Icons.edit;
    }
  }

  Container buildTopBar(BuildContext context, DetailsBloc bloc) {
    return Container(
      width: double.maxFinite,
      padding: EdgeInsets.symmetric(vertical: 8, horizontal: 8.0),
      decoration: BoxDecoration(
          border:
              Border(bottom: BorderSide(width: 1.0, color: Color(0xffEDF1F9)))),
      child: Stack(
        alignment: Alignment.center,
        children: <Widget>[
          Center(
              child: Text("Details",
                  textAlign: TextAlign.center,
                  style: GoogleFonts.montserrat(
                      textStyle: TextStyle(
                          fontSize: 15,
                          color: Color(0xff0F2253),
                          fontWeight: FontWeight.w500)))),
          Align(
            alignment: Alignment.bottomRight,
            child: GestureDetector(
              onTap: () {
//                detailsBloc.add(AddPropertyClickEvent());
                showEntryDialog(context, bloc);
              },
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Icon(Icons.add),
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget buildNameSection(DetailsEntry entry) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 8.0),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Icon(
            getEntryIcon(entry.type),
            color: Colors.tealAccent,
            size: 40,
          ),
          SizedBox(
            width: 8,
          ),
          Text(
            entry.value,
            style: GoogleFonts.montserrat(
                textStyle:
                    TextStyle(fontSize: 14, fontWeight: FontWeight.w600)),
          )
        ],
      ),
    );
  }

  void showEntryDialog(BuildContext context, DetailsBloc detailsBloc,
      {DetailsEntry detailsEntry}) {
    final TextEditingController nameController = TextEditingController();
    final TextEditingController valueController = TextEditingController();

    if (detailsEntry != null) {
      nameController.text = detailsEntry.name;
      valueController.text = detailsEntry.value;
    }

    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            actions: <Widget>[
              FlatButton(
                onPressed: () {
                  if (detailsEntry == null)
                    detailsBloc.add(AddDetailsEntryEvent(DetailsEntry(
                        id: "",
                        type: "custom",
                        name: nameController.text,
                        value: valueController.text)));
                  else {
                    detailsBloc.add(UpdateDetailsEntryEvent(
                        detailsEntry.copyWith(
                            name: nameController.text,
                            value: valueController.text)));
                  }
                  Navigator.of(context).pop();
                },
                child: Text(
                  detailsEntry == null ? "Add" : 'Update',
                  style: GoogleFonts.montserrat(
                      textStyle:
                          TextStyle(fontSize: 15.0, color: Colors.black)),
                ),
              )
            ],
            content: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Text(
                  detailsEntry == null ? 'Add details entry' : 'Update entry',
                  style: GoogleFonts.montserrat(
                      textStyle: TextStyle(
                          fontSize: 16,
                          color: Color(0xff0F2253),
                          fontWeight: FontWeight.w500)),
                ),
                TextField(
                  controller: nameController,
                  style: montserrat(),
                  decoration: hint("Name"),
                ),
                TextField(
                  controller: valueController,
                  style: montserrat(),
                  decoration: hint("Value"),
                ),
              ],
            ),
          );
        });
  }

  TextStyle montserrat() {
    return GoogleFonts.montserrat(textStyle: TextStyle(fontSize: 14.0));
  }

  InputDecoration hint(String hintText) {
    return InputDecoration(
        hintText: hintText,
        hintStyle:
            GoogleFonts.montserrat(textStyle: TextStyle(fontSize: 13.0)));
  }
}
