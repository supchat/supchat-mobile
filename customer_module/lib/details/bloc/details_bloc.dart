import 'dart:async';
import 'package:bloc/bloc.dart';
import '../../details/data/details_repository.dart';
import '../../details/model/Details.dart';

import 'details_event.dart';
import 'details_state.dart';
import 'details_state.dart';

class DetailsBloc extends Bloc<DetailsEvent, DetailsState> {
  final DetailsRepository _detailsRepository;

  DetailsBloc(this._detailsRepository) : super(InitialDetailsState());

  @override
  Stream<DetailsState> mapEventToState(
    DetailsEvent event,
  ) async* {
    if (event is DetailsOpenedEvent) {
      yield* mapDetailsOpenedToState(event);
    } else if (event is AddDetailsEntryEvent) {
      yield* mapAddEntryToState(event);
    } else if (event is UpdateDetailsEntryEvent) {
      yield* mapUpdateEntryToState(event);
    }
  }

  Stream<DetailsState> mapDetailsOpenedToState(
      DetailsOpenedEvent event) async* {
    try {
      final details = await _detailsRepository.getDetails();
      yield DetailsLoadedState(details);
    } catch (e) {
      print(e);
    }
  }

  Stream<DetailsState> mapAddEntryToState(AddDetailsEntryEvent event) async* {
    final entry = await _detailsRepository.addEntry(event.detailsEntry);
    var currentDetails = (state as DetailsLoadedState).details;
    final entries = currentDetails.entries;
    entries.add(entry);
    yield DetailsLoadedState(
        Details(currentDetails.clientId, entries, currentDetails.name));
  }

  Stream<DetailsState> mapUpdateEntryToState(
      UpdateDetailsEntryEvent event) async* {
    try {
      final entry = await _detailsRepository.updateEntry(event.detailsEntry);
      var currentDetails = (state as DetailsLoadedState).details;
      final entries = currentDetails.entries.map((e) => e).toList();
      final entryIndex =
          entries.indexWhere((element) => element.id == entry.id);
      entries[entryIndex] = entry;
      yield DetailsLoadedState(
          Details(currentDetails.clientId, entries, currentDetails.name));
    } catch (e) {
      print(e);
    }
  }
}
