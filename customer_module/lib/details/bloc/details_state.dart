import 'package:meta/meta.dart';
import '../../details/model/Details.dart';

@immutable
abstract class DetailsState {}

class InitialDetailsState extends DetailsState {}

class DetailsLoadedState extends DetailsState {
  final Details details;

  DetailsLoadedState(this.details);

}

