import 'package:meta/meta.dart';
import '../../details/model/Details.dart';

@immutable
abstract class DetailsEvent {}

class DetailsOpenedEvent extends DetailsEvent {
  final String userId;

  DetailsOpenedEvent(this.userId);
}

class AddDetailsEntryEvent extends DetailsEvent {
  final DetailsEntry detailsEntry;

  AddDetailsEntryEvent(this.detailsEntry);
}

class  UpdateDetailsEntryEvent extends DetailsEvent {
  final DetailsEntry detailsEntry;

  UpdateDetailsEntryEvent(this.detailsEntry);
}
