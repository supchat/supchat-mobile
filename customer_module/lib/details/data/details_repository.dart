import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:cloud_functions/cloud_functions.dart';
import 'package:cloud_functions/cloud_functions.dart';
import '../../details/model/Details.dart';
import 'package:cloud_functions/cloud_functions.dart';
import 'package:intl/intl.dart';

class DetailsRepository {
  final String _clientId;
  final String _customerId;

  DetailsRepository(this._clientId, this._customerId);

  final formatter = new DateFormat('hh:mm, dd-MM-yyyy');

  Future<Details> getDetails() async {
    FirebaseFirestore firestore = FirebaseFirestore.instance;
    final collection = await firestore
        .collection('customers/$_customerId/users/$_clientId/details')
        .get();
    final detailList = collection.docs
        .map((doc) => DetailsEntry(
            id: doc.id,
            type: doc['type'],
            name: "doc['name']",
            value: doc['value']))
        .toList();

    final userProperties =
        await firestore.doc("customers/$_customerId/users/$_clientId").get();
    final nameEntry = DetailsEntry(
        id: "null",
        value: userProperties['nickname'],
        type: "name",
        name: 'Name');
    final createdDate = (userProperties['timestamp'] as Timestamp).toDate();
    final createdEntry = DetailsEntry(
        id: "null",
        value: formatter.format(createdDate),
        type: "first_seen",
        name: 'First seen');

    final Timestamp lastSeenTs = userProperties['online']['last_changed'];
    final DateTime lastSeenDate = lastSeenTs.toDate();
    final lastSeenEntry = DetailsEntry(
        id: "null",
        value: formatter.format(lastSeenDate),
        type: "last_seen",
        name: 'Last seen');

    detailList.add(createdEntry);
    detailList.add(lastSeenEntry);
    return Details(_clientId, detailList, nameEntry);
  }

  Future<DetailsEntry> updateEntry(DetailsEntry entry) async {
    final HttpsCallable callable = FirebaseFunctions.instance.httpsCallable(
      'updateEntry',
    );

    final response = await callable.call(<String, dynamic>{
      'entryId': entry.id,
      'clientId': _clientId,
      'customerId': _customerId,
      'type': entry.type,
      'name': entry.name,
      'value': entry.value
    });

    return entry;
  }

  Future<DetailsEntry> addEntry(DetailsEntry entry) async {
    final HttpsCallable callable = FirebaseFunctions.instance.httpsCallable(
      'createEntry',
    );

    final response = await callable.call(<String, dynamic>{
      'clientId': _clientId,
      'customerId': _customerId,
      'type': entry.type,
      'name': entry.name,
      'value': entry.value
    });

    var newEntryId = response.data["data"];
    print('new entry id: ${newEntryId}');

    return entry.copyWith(id: newEntryId);
  }
}

extension on int {
  DateTime toDate() {
    return DateTime.fromMillisecondsSinceEpoch(this);
  }
}
