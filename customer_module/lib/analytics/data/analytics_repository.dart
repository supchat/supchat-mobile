import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/foundation.dart';

class AnalyticsRepository {
  final _analytics = FirebaseAnalytics();

  Future<void> setUserId(String userId) {
    return _analytics.setUserId(userId);
  }

  Future<void> logEvent(String event, {Map<String, String> params}) {
    if (kReleaseMode != true) return Future.delayed(Duration.zero);
    return _analytics.logEvent(name: event, parameters: params);
  }
}
