class Customer {
  final String id;
  final String name;
  final int timestamp;
  final String photoUrl;
  final String pushToken;

  Customer(this.id, this.name, this.timestamp, this.photoUrl, this.pushToken);
}
