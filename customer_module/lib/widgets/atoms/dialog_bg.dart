import 'package:flutter/material.dart';

class DialogBg extends StatelessWidget {
  final Widget child;
  final double padding;

  const DialogBg({Key key, this.child, this.padding = 50}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Container(
          padding: EdgeInsets.all(this.padding),
          width: 340.0,
          // height: 250,
          decoration: const BoxDecoration(
            borderRadius: const BorderRadius.all(
              Radius.circular(20.0),
            ),
            boxShadow: [
              const BoxShadow(
                color: Color.fromRGBO(56, 107, 201, 0.15),
                offset: Offset(0, 8),
                blurRadius: 20,
              ),
              const BoxShadow(
                color: Color.fromRGBO(56, 107, 201, 0.15),
                offset: Offset(0, -8),
                blurRadius: 20,
              ),
            ],
            color: const Color.fromRGBO(243, 248, 255, 1),
          ),
          child: child,
        ),
      ],
    );
  }
}
