import 'package:flutter/material.dart';

class TextFieldBg extends StatelessWidget {
  final Widget child;

  const TextFieldBg({Key key, this.child}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: AlignmentDirectional.center,
        width: 343,
        height: 60,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(
           Radius.circular(6),
          ),
          boxShadow: [
            BoxShadow(
                color: Color.fromRGBO(75, 78, 181, 0.1),
                offset: Offset(0, 7),
                blurRadius: 30)
          ],
          color: Color.fromRGBO(255, 255, 255, 1),
        ),
    child: child,);
  }
}
