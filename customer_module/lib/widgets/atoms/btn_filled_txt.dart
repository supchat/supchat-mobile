import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class FilledButtonText extends StatelessWidget {
  final String text;
  @required
  final VoidCallback onPressed;

  const FilledButtonText({Key key, this.text, this.onPressed})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FlatButton(
        color: Color(0xff8E91F4),
        highlightColor: Colors.grey.withAlpha(20),
        padding: EdgeInsets.all(20),
        onPressed: onPressed,
        child: Center(
          child: Text(text,
              textAlign: TextAlign.center,
              style: GoogleFonts.montserrat(
                  textStyle: TextStyle(
                      fontSize: 18,
                      color: Colors.white,
                      fontWeight: FontWeight.bold))),
        ),
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)));
  }
}
