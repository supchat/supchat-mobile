import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ButtonShadow extends StatelessWidget {
  final Widget child;

  const ButtonShadow({Key key, this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: AlignmentDirectional.center,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(16),
          topRight: Radius.circular(16),
          bottomLeft: Radius.circular(16),
          bottomRight: Radius.circular(16),
        ),
        boxShadow: [
          BoxShadow(
              color: Color.fromRGBO(69, 71, 137, 0.2),
              offset: Offset(0, 4),
              blurRadius: 15)
        ],
      ),
      child: child,
    );
  }
}
