import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class OutlinedButtonText extends StatelessWidget {
  final String text;
  @required
  final VoidCallback onPressed;

  const OutlinedButtonText({Key key, this.text, this.onPressed}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FlatButton(
        color: Colors.white,
        highlightColor: Colors.grey.withAlpha(20),
        padding: EdgeInsets.all(20),
        onPressed: onPressed,
        child: Center(
          child: Text(text,
              textAlign: TextAlign.center,
              style: GoogleFonts.montserrat(
                  textStyle: TextStyle(
                      fontSize: 18,
                      color: Color(0xff8E91F4),
                      fontWeight: FontWeight.bold))),
        ),
        shape: RoundedRectangleBorder(
            side: BorderSide(
                color: Color(0xff8E91F4),
                width: 2,
                style: BorderStyle.solid),
            borderRadius: BorderRadius.circular(15)));
  }
}
