import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:supchat/widgets/atoms/dialog_bg.dart';
import 'package:google_fonts/google_fonts.dart';

enum Result {
  yes, no
}

class YesNoDialog extends StatelessWidget {
  final String message;
  final String yesButtonText;
  final String noButtonText;

  const YesNoDialog({
    Key key,
    this.message,
    this.yesButtonText,
    this.noButtonText,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      elevation: 0.0,
      contentPadding: const EdgeInsets.all(16.0),
      backgroundColor: Colors.transparent,
      insetPadding: const EdgeInsets.all(10.0),
      content: DialogBg(
        padding: 24.0,
        child: Material(
          color: Colors.transparent,
          child: Column(
            children: <Widget>[
              Text(
                message,
                textAlign: TextAlign.center,
                style: const TextStyle(color: Colors.black54, fontSize: 20),
              ),
              _buildOptions(context),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildOptions(context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Padding(
          padding: const EdgeInsets.only(left: 12.0, top: 24.0),
          child: SimpleDialogOption(
            onPressed: () {
              Navigator.pop(context, Result.no);
            },
            child: Text(
              noButtonText,
              style: GoogleFonts.montserrat(
                  textStyle: TextStyle(
                      color: Color(0xff8E91F4),
                      fontSize: 16,
                      fontWeight: FontWeight.w600)),
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(right: 12.0, top: 24.0),
          child: SimpleDialogOption(
            onPressed: () {
              Navigator.pop(context, Result.yes);
            },
            child: Text(
              yesButtonText,
              style: GoogleFonts.montserrat(
                  textStyle: TextStyle(
                      color: Color(0xff8E91F4),
                      fontSize: 16,
                      fontWeight: FontWeight.w600)),
            ),
          ),
        ),
      ],
    );
  }
}
