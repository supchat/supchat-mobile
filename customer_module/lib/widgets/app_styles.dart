import 'package:flutter/cupertino.dart';

abstract class TextStyles {
  static final authTitle = TextStyle(
      fontSize: 36,
      height: 1.5,
      color: Color(0xff0F2253),
      fontWeight: FontWeight.w900);

  static final dialogText = TextStyle(
      height: 1.6,
      fontSize: 18,
      fontWeight: FontWeight.w500,
      color: Color(0xff90969E));
}
