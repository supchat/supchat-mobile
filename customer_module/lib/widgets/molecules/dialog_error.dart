import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:supchat/widgets/atoms/dialog_bg.dart';
import 'package:google_fonts/google_fonts.dart';

import '../app_styles.dart';

class ErrorDialog extends StatelessWidget {
  final String text;

  const ErrorDialog({Key key, this.text}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      elevation: 0.0,
      contentPadding: EdgeInsets.all(16),
      backgroundColor: Colors.transparent,
      insetPadding: EdgeInsets.all(10),
      content: DialogBg(
        child: Center(
            child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Icon(
              Icons.error_outline,
              color: Colors.red,
              size: 76,
            ),
            SizedBox(
              height: 22,
            ),
            Text(text,
                textAlign: TextAlign.center,
                style: GoogleFonts.montserrat(textStyle: TextStyles.dialogText))
          ],
        )),
      ),
    );
  }

  static show(BuildContext context, String text) {
    showDialog(
        barrierDismissible: true,
        barrierColor: Colors.white.withOpacity(0.4),
        context: context,
        builder: (context) => ErrorDialog(
              text: text,
            ));
  }
}
