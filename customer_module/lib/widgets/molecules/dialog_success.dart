import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:supchat/widgets/atoms/dialog_bg.dart';
import 'package:google_fonts/google_fonts.dart';

import '../app_styles.dart';

class SuccessDialog extends StatelessWidget {
  final String text;

  const SuccessDialog({Key key, this.text}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      elevation: 0.0,
      contentPadding: const EdgeInsets.all(16.0),
      backgroundColor: Colors.transparent,
      insetPadding: const EdgeInsets.all(10.0),
      content: DialogBg(
        child: Center(
            child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            SizedBox(
                width: 76.0,
                height: 76.0,
                child: SvgPicture.asset('assets/icons/ic_success_mark.svg')),
            const SizedBox(height: 22.0),
            Text(text,
                textAlign: TextAlign.center,
                style: GoogleFonts.montserrat(textStyle: TextStyles.dialogText))
          ],
        )),
      ),
    );
  }

  static show(
      BuildContext context, String text, Function onTimeoutDismiss) async {
    showDialog(
        barrierDismissible: false,
        barrierColor: Colors.white.withOpacity(0.4),
        context: context,
        builder: (context) => SuccessDialog(
              text: text,
            ));
    await Future.delayed(Duration(seconds: 1));
    onTimeoutDismiss.call();
  }
}
