import 'package:flutter/material.dart';
import 'package:supchat/widgets/atoms/btn_outlined_txt.dart';
import 'package:supchat/widgets/atoms/btn_shadow.dart';
import 'package:supchat/widgets/atoms/btn_filled_txt.dart';

class OutlinedButton extends StatelessWidget {
  final String text;
  @required
  final VoidCallback onPressed;

  const OutlinedButton({Key key, this.text, this.onPressed}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ButtonShadow(
      child: OutlinedButtonText(
        text: text,
        onPressed: onPressed,
      ),
    );
  }
}
