import 'package:flutter/material.dart';
import 'package:supchat/widgets/atoms/btn_shadow.dart';
import 'package:supchat/widgets/atoms/btn_filled_txt.dart';

class FilledButton extends StatelessWidget {
  final String text;
  @required
  final VoidCallback onPressed;

  const FilledButton({Key key, this.text, this.onPressed}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ButtonShadow(
      child: FilledButtonText(
        text: text,
        onPressed: onPressed,
      ),
    );
  }
}
