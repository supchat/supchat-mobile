import 'package:google_fonts/google_fonts.dart';
import 'package:flutter/material.dart';
import 'package:supchat/widgets/atoms/txt_field_bg.dart';

class InputText extends StatefulWidget {
  final String title;
  final String hint;
  final TextEditingController controller;
  final bool isEmail;
  final bool isPassword;

  const InputText(
      {Key key,
      this.title,
      this.hint,
      this.controller,
      this.isEmail = false,
      this.isPassword = false})
      : super(key: key);

  @override
  _InputTextState createState() => _InputTextState();
}

class _InputTextState extends State<InputText> {
  bool isPasswordVisible;

  @override
  void initState() {
    isPasswordVisible = true;
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        Text(
          widget.title,
          textAlign: TextAlign.left,
          style: GoogleFonts.montserrat(
              textStyle: TextStyle(
                  color: Color.fromRGBO(144, 149, 158, 1),
                  fontSize: 16,
                  letterSpacing: 0,
                  fontWeight: FontWeight.normal,
                  height: 3.1875)),
        ),
        SizedBox(
          height: 4,
        ),
        TextFieldBg(
          child: TextField(
            controller: widget.controller,
            obscureText: widget.isPassword && isPasswordVisible,
            textAlignVertical: widget.isPassword
                ? TextAlignVertical.center
                : TextAlignVertical.top,
            style: GoogleFonts.montserrat(
                textStyle: TextStyle(
                    fontWeight: FontWeight.w400, color: Color(0xff596278))),
            cursorColor: Color(0xff8E91F4),
            keyboardType: widget.isEmail
                ? TextInputType.emailAddress
                : widget.isPassword
                    ? TextInputType.visiblePassword
                    : TextInputType.text,
            decoration: InputDecoration(
                border: InputBorder.none,
                focusedBorder: InputBorder.none,
                enabledBorder: InputBorder.none,
                errorBorder: InputBorder.none,
                alignLabelWithHint: false,
                disabledBorder: InputBorder.none,
                contentPadding: EdgeInsets.symmetric(horizontal: 15),
                hintStyle: GoogleFonts.montserrat(
                    textStyle: TextStyle(
                        fontWeight: FontWeight.w400, color: Color(0xffD1D5D6))),
                hintText: widget.hint,
                suffixIcon: widget.isPassword
                    ? IconButton(
                        icon: Icon(
                          // Based on passwordVisible state choose the icon
                          widget.isPassword && isPasswordVisible
                              ? Icons.visibility
                              : Icons.visibility_off,
                          color: Color(0xffD1D5D6),
                        ),
                        onPressed: () {
                          setState(() {
                            isPasswordVisible = !isPasswordVisible;
                          });
                        },
                      )
                    : null),
          ),
        ),
      ],
    );
  }
}
