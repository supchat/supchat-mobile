import 'package:flutter/material.dart';
import 'package:supchat/widgets/atoms/dialog_bg.dart';
import 'package:google_fonts/google_fonts.dart';

import '../app_styles.dart';

class ProgressDialog extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      elevation: 0.0,
      contentPadding: EdgeInsets.all(16),
      backgroundColor: Colors.transparent,
      insetPadding: EdgeInsets.all(10),
      content: DialogBg(
        child: Center(
            child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            SizedBox(
                width: 76,
                height: 76,
                child: CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation<Color>(Color(0xff6CA0FF)),
                  strokeWidth: 5,
                )),
            SizedBox(
              height: 22,
            ),
            Text('Please wait...',
                style: GoogleFonts.montserrat(textStyle: TextStyles.dialogText))
          ],
        )),
      ),
    );
  }

  static show(BuildContext context) {
    showDialog(
        barrierDismissible: false,
        barrierColor: Colors.white.withOpacity(0.4),
        context: context,
        builder: (context) => ProgressDialog());
  }
}
