import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:image_picker/image_picker.dart';
import 'package:logger/logger.dart';
import 'package:supchat/analytics/data/analytics_repository.dart';
import 'package:supchat/billing/billing_screen.dart';
import 'package:supchat/billing/data/billing_repository.dart';
import 'package:supchat/landing/landing_screen.dart';
import 'package:supchat/profile/edit_name_dialog.dart';
import 'package:supchat/profile/photo_source_dialog.dart';
import 'package:supchat/profile/terms_screen.dart';
import 'package:supchat/sl/getIt.dart';
import 'package:supchat/user/data/customer_user_repository.dart';
import 'package:supchat/widgets/atoms/content_container.dart';
import 'package:supchat/widgets/molecules/btn_filled.dart';
import 'package:supchat/widgets/molecules/dialog_error.dart';
import 'package:supchat/widgets/molecules/dialog_progress.dart';
import 'package:supchat/widgets/yes_no_dialog.dart';

import 'bloc/settings_cubit.dart';

class SettingsScreen extends StatelessWidget {
  final String apiKey;

  final GlobalKey<ScaffoldMessengerState> _scaffoldMessengerKey =
      GlobalKey<ScaffoldMessengerState>();

  final SettingsCubit _settingsCubit = SettingsCubit(
    getIt<CustomerUserRepository>(),
    getIt<Logger>(),
    getIt<BillingRepository>(),
  );

  SettingsScreen({Key key, this.apiKey}) : super(key: key) {
    _settingsCubit.loadUser();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldMessengerKey,
      body: BlocConsumer<SettingsCubit, SettingsState>(
        cubit: _settingsCubit,
        listener: (BuildContext context, SettingsState state) {
          if (state is ProgressState) {
            ProgressDialog.show(context);
          } else if (state is HiddenProgressState) {
            Navigator.pop(context);
          } else if (state is ErrorState) {
            ErrorDialog.show(context, state.text);
          } else if (state is LoggedOutState) {
            Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(
                builder: (context) => LandingScreen(),
              ),
              (route) => false,
            );
          }
        },
        buildWhen: (SettingsState previous, SettingsState current) =>
            current is UserLoaded,
        builder: (context, state) => Stack(
          children: [
            Container(
                width: double.maxFinite,
                color: Color(0xff8E91F4),
                child: SvgPicture.asset(
                  'assets/profile_bg.svg',
                  fit: BoxFit.fill,
                )),
            Container(
              margin: const EdgeInsets.only(top: 24.0),
              child: Material(
                color: Colors.transparent,
                child: IconButton(
                  icon: const Icon(Icons.arrow_back_rounded),
                  tooltip: 'Back button',
                  color: Colors.yellow,
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
              ),
            ),
            Container(
              alignment: Alignment.topRight,
              margin: const EdgeInsets.only(top: 24.0),
              child: Material(
                color: Colors.transparent,
                child: IconButton(
                  icon: const Icon(Icons.logout),
                  tooltip: 'Log out button',
                  color: Colors.yellow,
                  onPressed: () {
                    _openLogOutDialog(context);
                  },
                ),
              ),
            ),
            Align(
              alignment: AlignmentDirectional.topCenter,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: [
                  _buildUserAvatar(context,
                      (state is UserLoaded) ? state.user.photoUrl : null),
                  const SizedBox(height: 16.0),
                  GestureDetector(
                    onTap: () => _openNameInputDialog(context,
                        (state is UserLoaded) ? state.user.nickname : null),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          (state is UserLoaded)
                              ? state.user.nickname ?? "Name not set"
                              : 'Name not set',
                          textAlign: TextAlign.center,
                          style: GoogleFonts.montserrat(
                              textStyle: TextStyle(
                            color: Color.fromRGBO(255, 255, 255, 1),
                            fontSize: 24,
                            letterSpacing: 0,
                            fontWeight: FontWeight.bold,
                          )),
                        ),
                        const SizedBox(width: 8.0),
                        const Icon(Icons.edit, color: Colors.white)
                      ],
                    ),
                  ),
                ],
              ),
            ),
            BlocBuilder<SettingsCubit, SettingsState>(
              cubit: _settingsCubit,
              builder: (BuildContext context, SettingsState state) => Container(
                color: Colors.transparent,
                margin: EdgeInsets.only(top: 270),
                child: ContentContainer(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        _buildApiKeyLabel(context),
                        const SizedBox(height: 24.0),
                        _buildMembershipPart(context, state),
                        _buildLegalRow(context),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildUserAvatar(BuildContext context, String photoUrl) {
    return Container(
      margin: const EdgeInsets.only(top: 72.0),
      child: Stack(
        children: [
          Container(
            height: 120.0,
            width: 120.0,
            padding: const EdgeInsets.all(4.0),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(79.0),
                border: Border.all(width: 3.0, color: Color(0xffF8D7A5))),
            child: photoUrl == null || photoUrl.isEmpty
                ? Image.asset('assets/images/ic_empty_avatar.png')
                : Material(
                    borderRadius: const BorderRadius.all(Radius.circular(60.0)),
                    clipBehavior: Clip.antiAlias,
                    child: Image(
                      image: NetworkImage(photoUrl),
                      width: 120.0,
                      height: 120.0,
                      fit: BoxFit.cover,
                    ),
                  ),
          ),
          GestureDetector(
            onTap: () {
              _openPhotoSourceDialog(context);
            },
            child: Container(
              height: 120.0,
              width: 120.0,
              padding: const EdgeInsets.all(4.0),
              child: SvgPicture.asset("assets/icons/ic_upload_photo.svg"),
              alignment: AlignmentDirectional.bottomEnd,
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildMembershipPart(BuildContext context, SettingsState state) {
    return state is UserLoaded && state.isPaid
        ? Text("Already subscribed to membership",
            textAlign: TextAlign.center,
            style: GoogleFonts.montserrat(
              textStyle: TextStyle(fontSize: 22),
            ))
        : Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20.0),
            child: FilledButton(
              text: 'Buy membership',
              onPressed: () async {
                // Navigator.of(context).push(MaterialPageRoute(
                //     builder: (context) => BillingScreen()));
                getIt<AnalyticsRepository>().logEvent("buy_button_click");
                final result = await BillingScreen.showInDialog(context);
                _settingsCubit.loadUser();
              },
            ),
          );
  }

  Widget _buildApiKeyLabel(context) {
    return Material(
      color: Colors.transparent,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const SizedBox(height: 10.0),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              const SizedBox(width: 22.0),
              Text(
                "API KEY",
                style: GoogleFonts.montserrat(
                    textStyle: const TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.bold,
                        color: Color(0xff55658A))),
              ),
              Spacer(),
              IconButton(
                  padding:
                      const EdgeInsets.only(left: 8.0, top: 8.0, bottom: 8.0),
                  icon: SvgPicture.asset("assets/icons/ic_copy.svg"),
                  onPressed: () {
                    Clipboard.setData(
                      ClipboardData(text: this.apiKey),
                    );
                    ScaffoldMessenger.of(context).showSnackBar(
                      const SnackBar(
                        content: const Text('Copied to Clipboard'),
                      ),
                    );
                  }),
              SizedBox(
                width: 16,
              ),
            ],
          ),
          Padding(
            padding: const EdgeInsets.only(left: 22.0, right: 22.0),
            child: SelectableText(
              this.apiKey,
              style: GoogleFonts.montserrat(
                  textStyle: const TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.normal,
                      color: Color(0xff55658A))),
            ),
          ),
          const SizedBox(
            height: 8.0,
          )
        ],
      ),
    );
  }

  Widget _buildLegalRow(context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Padding(
          padding: const EdgeInsets.only(left: 12.0, top: 24.0),
          child: TextButton(
            onPressed: () {
              Navigator.of(context).push(MaterialPageRoute(
                builder: (context) => TermsScreen(type: Type.privacy),
              ));
            },
            child: Text(
              "Privacy Policy",
              style: GoogleFonts.montserrat(
                  textStyle: TextStyle(
                      color: Color(0xff8E91F4),
                      fontSize: 16,
                      fontWeight: FontWeight.w600)),
            ),
            style: TextButton.styleFrom(
              backgroundColor: Colors.transparent,
              onSurface: Colors.grey,
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(right: 12.0, top: 24.0),
          child: TextButton(
            onPressed: () {
              Navigator.of(context).push(MaterialPageRoute(
                builder: (context) => TermsScreen(type: Type.terms),
              ));
            },
            child: Text("Terms Of Use",
                style: GoogleFonts.montserrat(
                    textStyle: TextStyle(
                        color: Color(0xff8E91F4),
                        fontSize: 16,
                        fontWeight: FontWeight.w600))),
            style: TextButton.styleFrom(
              backgroundColor: Colors.transparent,
              onSurface: Colors.grey,
            ),
          ),
        ),
      ],
    );
  }

  Future _getImage(ImageSource source) async {
    try {
      final pickedFile = await ImagePicker().getImage(source: source);
      if (pickedFile != null) {
        print(pickedFile?.path);
        _settingsCubit.updatePhoto(pickedFile?.path, apiKey);
      } else {
        print('imageFile == null');
      }
    } catch (e) {
      print('getImage exception $e');
    }
  }

  Future<void> _openNameInputDialog(
      BuildContext context, String currentName) async {
    final newName = await showDialog<String>(
      context: context,
      builder: (BuildContext context) {
        return EditNameDialog(name: currentName);
      },
    );

    if (newName != null && newName.isNotEmpty) {
      _settingsCubit.updateNickname(newName);
    }
  }

  Future<void> _openPhotoSourceDialog(BuildContext context) async {
    final source = await showDialog<ImageSource>(
        context: context,
        builder: (BuildContext context) {
          return PhotoSourceDialog();
        },
    );
    _getImage(source);
  }

  Future<void> _openLogOutDialog(BuildContext context) async {
    final result = await showDialog<Result>(
      context: context,
      builder: (BuildContext context) {
        return YesNoDialog(
          message: 'Are you sure you want to log out?',
          yesButtonText: 'Log Out',
          noButtonText: 'Cancel',
        );
      },
    );
    if (result == Result.yes) {
      _settingsCubit.logOut();
    }
  }
}
