import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:image_picker/image_picker.dart';

class PhotoSourceDialog extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      buttonPadding:
          EdgeInsets.only(left: 10.0, right: 10.0, top: 0, bottom: 0),
      content: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Text(
            'New profile picture',
            style: GoogleFonts.montserrat(
              textStyle: const TextStyle(
                fontSize: 16,
                color: Color(0xff0F2253),
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          const SizedBox(height: 24.0),
          FlatButton(
            onPressed: () {
              Navigator.pop(context, ImageSource.camera);
            },
            child: Text(
              'Take a photo',
              style: GoogleFonts.montserrat(
                textStyle: const TextStyle(
                  color: Color(0xff8E91F4),
                  fontSize: 16,
                  fontWeight: FontWeight.w600,
                ),
              ),
            ),
          ),
          const Flexible(child: Divider(thickness: 1.0, color: Colors.grey)),
          FlatButton(
            onPressed: () {
              Navigator.pop(context, ImageSource.gallery);
            },
            child: Text(
              'Choose from gallery',
              style: GoogleFonts.montserrat(
                textStyle: const TextStyle(
                  color: Color(0xff8E91F4),
                  fontSize: 16,
                  fontWeight: FontWeight.w600,
                ),
              ),
            ),
          ),
          const Flexible(child: Divider(thickness: 1.0, color: Colors.grey)),
          FlatButton(
            onPressed: () {
              Navigator.of(context).pop();
            },
            child: Text(
              'Cancel',
              style: GoogleFonts.montserrat(
                textStyle: const TextStyle(
                  color: Color(0xff8E91F4),
                  fontSize: 16,
                  fontWeight: FontWeight.w600,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
