import 'package:bloc/bloc.dart';
import 'package:logger/logger.dart';
import 'package:meta/meta.dart';
import 'package:supchat/billing/data/billing_repository.dart';
import 'package:supchat/user/data/customer_user_repository.dart';
import 'package:supchat/user/model/user.dart';

part 'settings_state.dart';

class SettingsCubit extends Cubit<SettingsState> {
  final CustomerUserRepository _userRepository;
  final BillingRepository _billingRepository;
  final Logger _logger;

  SettingsCubit(this._userRepository, this._logger, this._billingRepository) : super(SettingsInitial());

  void loadUser() async {
    final user = await _userRepository.getUser();
    await _billingRepository.setup();
    final paymentInfo = await _billingRepository.getPurchaserInfo();
    final membership = paymentInfo.entitlements.all["membership"];
    final isPaid = membership?.isActive == true;
    emit(UserLoaded(user, isPaid));
  }

  void updateNickname(String newNickname) async {
    if (state is! UserLoaded) return;
    final user = (state as UserLoaded).user;
    final isPaid = (state as UserLoaded).isPaid;
    final newUser = user.copyWith(nickname: newNickname);
    try {
      emit(ProgressState());
      await _userRepository.update(newUser);
      emit(HiddenProgressState());
      emit(UserLoaded(newUser, isPaid));
    } catch(e) {
      emit(HiddenProgressState());
      emit(ErrorState("Error while updating your username, please check your Internet connection or try again later."));
      emit(UserLoaded(user, isPaid));
    }
  }

  void updatePhoto(String newPhoto, String customerId) async {
    if (state is! UserLoaded) return;
    final user = (state as UserLoaded).user;
    final isPaid = (state as UserLoaded).isPaid;
    try {
      emit(ProgressState());
      final photoUrl = await _userRepository.uploadNewPhoto(
          newPhoto, customerId);
      final newUser = user.copyWith(photoUrl: photoUrl);
      await _userRepository.update(newUser);
      emit(HiddenProgressState());
      emit(UserLoaded(newUser, isPaid));
    } catch(e) {
      emit(HiddenProgressState());
      emit(ErrorState("Error while uploading your new profile picture, please check your Internet connection or try again later."));
      emit(UserLoaded(user, isPaid));
    }
  }

  void logOut() async {
    await _userRepository.logOut();
    emit(LoggedOutState());
  }
}
