part of 'settings_cubit.dart';

@immutable
abstract class SettingsState {}

class SettingsInitial extends SettingsState {}

class UserLoaded extends SettingsState {
  final User user;
  final bool isPaid;

  UserLoaded(this.user, this.isPaid);
}

class ProgressState extends SettingsState {}

class HiddenProgressState extends SettingsState {}

class ErrorState extends SettingsState {
  final String text;

  ErrorState(this.text);

  @override
  String toString() {
    return "ErrorState: $text";
  }
}

class LoggedOutState extends SettingsState {}
