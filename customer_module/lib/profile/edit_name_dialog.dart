import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:google_fonts/google_fonts.dart';

class EditNameDialog extends StatefulWidget {
  final String name;

  const EditNameDialog({Key key, this.name}) : super(key: key);

  @override
  _EditNameDialogState createState() => _EditNameDialogState();
}

class _EditNameDialogState extends State<EditNameDialog> {
  final nameController = TextEditingController();
  bool isValid = true;

  @override
  void initState() {
    super.initState();
    if (widget.name != null) {
      nameController.text = widget.name;
    }
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      buttonPadding:
          EdgeInsets.only(left: 10.0, right: 10.0, top: 0, bottom: 0),
      content: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Text(
            'Update name',
            style: GoogleFonts.montserrat(
              textStyle: TextStyle(
                  fontSize: 16,
                  color: Color(0xff0F2253),
                  fontWeight: FontWeight.w500),
            ),
          ),
          const SizedBox(height: 12.0),
          TextField(
            controller: nameController,
            textCapitalization: TextCapitalization.words,
            style: GoogleFonts.montserrat(
                textStyle: const TextStyle(fontSize: 16)),
            decoration: InputDecoration(
              hintText: "Name",
              hintStyle: GoogleFonts.montserrat(
                  textStyle: const TextStyle(fontSize: 16),
                  color: Colors.black26),
              errorText: isValid ? null : 'Your name cannot be empty',
            ),
            onChanged: (text) {
              setState(() {
                isValid = true;
              });
            },
          ),
          const SizedBox(height: 18.0),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              FlatButton(
                onPressed: () {
                  Navigator.of(context).pop();
                },
                child: Text(
                  'Cancel',
                  style: GoogleFonts.montserrat(
                    textStyle: TextStyle(
                        color: Color(0xff8E91F4),
                        fontSize: 16,
                        fontWeight: FontWeight.w600),
                  ),
                ),
              ),
              FlatButton(
                onPressed: () {
                  if (nameController.value.text != null && nameController.value.text.trim().isNotEmpty) {
                    Navigator.pop(context, nameController.value.text.trim());
                  } else {
                    setState(() {
                      isValid = false;
                    });
                  }
                },
                child: Text(
                  'Update',
                  style: GoogleFonts.montserrat(
                    textStyle: TextStyle(
                        color: Color(0xff8E91F4),
                        fontSize: 16,
                        fontWeight: FontWeight.w600),
                  ),
                ),
              ), // button 2
            ],
          ),
        ],
      ),
    );
  }
}
