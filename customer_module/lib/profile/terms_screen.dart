import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:webview_flutter/webview_flutter.dart';

class TermsScreen extends StatelessWidget {
  final Type type;

  WebViewController _controller;

  TermsScreen({Key key, this.type}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(
          children: [
            Stack(
              children: [
                Align(
                  alignment: AlignmentDirectional.centerStart,
                  child: IconButton(
                    padding: const EdgeInsets.all(16.0),
                    icon: Icon(Icons.arrow_back),
                    onPressed: () {
                      Navigator.pop(context);
                    },
                  ),
                ),
                Positioned.fill(
                  child: Center(
                    child: Text(
                        type == Type.privacy
                            ? "Privacy Policy"
                            : "Terms Of Use",
                        style: GoogleFonts.montserrat(
                            textStyle: TextStyle(fontSize: 20))
                        // textAlign: TextAlign.center,
                        ),
                  ),
                )
              ],
            ),
            Expanded(
                child: WebView(
              initialUrl: 'about:blank',
              onWebViewCreated: (WebViewController webViewController) {
                _controller = webViewController;
                _loadHtmlFromAssets();
              },
            ))
          ],
        ),
      ),
    );
  }

  _loadHtmlFromAssets() async {
    String fileText = await rootBundle.loadString(
        'assets/docs/${type == Type.privacy ? "privacy" : "terms"}.html');
    _controller.loadUrl(Uri.dataFromString(fileText,
            mimeType: 'text/html', encoding: Encoding.getByName('utf-8'))
        .toString());
  }
}

enum Type { privacy, terms }
