import 'package:flutter/material.dart';
import 'package:supchat/logging/logging.dart';
import 'package:supchat_chat/chat_config.dart';

import 'main/CustomerApp.dart';
import 'sl/getIt.dart';

void main() {
  initDependencies(ChatConfig(isLibrary: false));
  initBlocLogging();
  runApp(CustomerApp());
}
