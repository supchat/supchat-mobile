import 'dart:async';
import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:logger/logger.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:supchat/analytics/data/analytics_repository.dart';
import 'package:supchat/user/data/customer_user_repository.dart';
import 'package:supchat_chat/chat_config.dart';
import 'package:supchat_chat/chat/data/chat_repository.dart';
import 'package:supchat_chat/chat/model/chat_info.dart';
import 'package:supchat_chat/chat/model/message.dart';

class CustomerChatRepository extends ChatRepository {
  final ChatConfig _config;

  FirebaseFirestore _firestore;

  final Logger _logger;

  final CustomerUserRepository _customerUserRepository;
  final AnalyticsRepository _analyticsRepository;

  CustomerChatRepository(
      this._config, this._logger, this._customerUserRepository, this._analyticsRepository);

  Future<List<Message>> getChatHistory(String chatId) async {
    final customerId = await _getCustomerId();

    final chatMessagesPath = _getChatMessagesPath(customerId, chatId);

    final query = _firestore
        .collection(chatMessagesPath)
        .orderBy('timestamp', descending: true);

    final cachedMessages = await query.get(GetOptions(source: Source.cache));

    if (cachedMessages.size != 0) {
      _logger.i(
          'Firebase getChatHistory: cached: ${cachedMessages.metadata.isFromCache}');
      final localMessages = cachedMessages.toMessages(customerId);
      final lastMessageTs = localMessages.first.time;

      final remoteMessagesSnapshot = await query
          .where('timestamp', isGreaterThan: Timestamp.fromDate(lastMessageTs))
          .get(GetOptions(source: Source.server));
      final remoteMessages = remoteMessagesSnapshot.toMessages(customerId);

      _logger.i('Firebase remoteMessages: size: ${remoteMessages.length}');
      remoteMessages.addAll(localMessages);
      await _saveLastMessageTimestamp(chatId, remoteMessages.first.time);
      return remoteMessages;
    } else {
      final serverMessages = await query.get(GetOptions(source: Source.server));
      _logger.i(
          'Firebase getChatHistory: cached: ${cachedMessages.metadata.isFromCache}');

      final messages = serverMessages.toMessages(customerId);
      await _saveLastMessageTimestamp(chatId, messages.first.time);
      return messages;
    }
  }

  Future<dynamic> _saveLastMessageTimestamp(
      String chatId, DateTime time) async {
    final preferences = await SharedPreferences.getInstance();
    _logger.i("Firebase _saveLastMessageTimestamp, ${time.toIso8601String()}");

    await preferences.setString(
        '$chatId.lastMessageTimestamp}', time.toIso8601String());
  }

  Future<DateTime> _getLastMessageTimestamp(String chatId) async {
    final preferences = await SharedPreferences.getInstance();
    final stringDate =
        preferences.getString('$chatId.lastMessageTimestamp}') ?? "";
    final timestamp = DateTime.tryParse(stringDate);
    _logger.i("Firebase _getLastMessageTimestamp, $timestamp");
    return timestamp;
  }

  Future<dynamic> markChatSeen(String chatId) async {
    final customer = await _customerUserRepository.getUser();
    final customerId = customer.id;
    final chatPath = _getChatPath(customerId, chatId);

    _logger.i('markChatSeen path: $chatPath');

    try {
      return _firestore
          .doc(chatPath)
          .set({'unreadMessagesCount': 0}, SetOptions(merge: true));
    } catch (e) {
      print('markChatSeen errr: $e');
    }
    return Future.value('');
  }

  Stream<List<Message>> onNewMessages(String chatId) async* {
    final customerId = await _getCustomerId();

    final chatMessagesPath = _getChatMessagesPath(customerId, chatId);

    var collectionReference = _firestore
        .collection(chatMessagesPath)
        .orderBy('timestamp', descending: true);

    final lastMessageTimestamp = await _getLastMessageTimestamp(chatId);
    if (lastMessageTimestamp != null) {
      collectionReference = collectionReference.where('timestamp',
          isGreaterThan: Timestamp.fromDate(lastMessageTimestamp));
    }

    var stream = collectionReference.snapshots();

    if (lastMessageTimestamp == null) {
      //we'll receive 1st result with all new messages, so we need to skip it
      stream = stream.skip(1);
    }

    yield* stream
        .map((snapshot) => snapshot.docChanges
            .where((change) => change.type == DocumentChangeType.added))
        .map((changes) => changes.map((change) => change.doc).toList())
        .where((list) => list.isNotEmpty)
        .map((docs) {
      _logger.i(
          "Firebase onNewMessages, size: ${docs.length}; first: ${docs.first.dateTimeFromTimestamp()}");
      _analyticsRepository.logEvent("message_received");

      _saveLastMessageTimestamp(chatId, docs.first.dateTimeFromTimestamp());
      return docs
          .map((doc) => Message(
              idFrom: doc['idFrom'],
              content: doc['content'],
              type: doc['type'],
              time: doc.dateTimeFromTimestamp(),
              isMy: doc['idFrom'] == customerId,
              isLast: docs.first == doc))
          .toList();
    });
  }

  Future<String> _getCustomerId() async {
    final customer = await _customerUserRepository.getUser();
    return customer.id;
  }

  Future<dynamic> sendMessage(String chatId, Message message) async {
    _analyticsRepository.logEvent("message_sent");

    setChatLastMessage(chatId, message);

    final customerId = await _getCustomerId();

    return _firestore.collection(_getChatMessagesPath(customerId, chatId)).add({
      'idFrom': message.idFrom,
      'content': message.content,
      'type': message.type,
      'customerId': customerId,
      'chatId': chatId,
      'timestamp': FieldValue.serverTimestamp()
    });
  }

  String _getChatMessagesPath(String customerId, String chatId) {
    return '${_getChatPath(customerId, chatId)}/messages';
  }

  String _getChatPath(String customerId, String chatId) {
    return 'customers/$customerId/chats/$chatId';
  }

  Future<String> uploadFile(dynamic data) async {
    final customerId = await _getCustomerId();
    final result = await FirebaseStorage.instance
        .ref('$customerId/${DateTime.now().microsecondsSinceEpoch}')
        .putFile(File(data));
    return await result.ref.getDownloadURL();
  }

  Future<dynamic> setChatLastMessage(String chatId, Message message) async {
    final customer = await _customerUserRepository.getUser();
    final customerId = customer.id;
    final chatPath = _getChatPath(customerId, chatId);

    return _firestore.doc(chatPath).set({
      'unreadMessagesCount': FieldValue.increment(1),
      'lastMessage': {
        'content': message.content,
        'timestamp': FieldValue.serverTimestamp(),
        'type': message.type
      }
    }, SetOptions(merge: true));
  }

  Stream<dynamic> updateOnline(String id, {String chatMateId}) {
    return Stream.value(34);
  }

  Stream<ChatInfo> loadChatInfo(String chatId) async* {
    final customerId = await _getCustomerId();

    final onlineStatusPath = '${getCustomerInfoPath(customerId)}/users/$chatId/';
    print('loadChatInfo $onlineStatusPath');

    yield* _firestore.doc(onlineStatusPath).snapshots().map((document) {
      print('loadChatInfo ${document.data()}');
      return ChatInfo(
          id: document.id,
          name: document.data()['nickname'] ?? '',
          online: document?.data != null
              ? Online(
                  online: document['online']['state'],
                  lastUpdate: DateTime.now())
              : null);
    });
  }

  String _getOnlineStatusCustomerPath(String customerId) {
    return '${getCustomerInfoPath(customerId)}/online/';
  }

  String getCustomerInfoPath(String customerId) {
    return 'customers/$customerId';
  }

  String _getOnlineStatusClientPath(String customerId, String clientId) {
    return '${getUserInfoPath(customerId, clientId)}/online/';
  }

  String getUserInfoPath(String customerId, String clientId) {
    return 'customers/$customerId/users/$clientId';
  }

  @override
  void close() {}

  @override
  Future<dynamic> initialize() async {
    await _initFirebase();
    _firestore = FirebaseFirestore.instance;
  }

  _initFirebase() async {
    final appName = 'supchat';
    try {
      await Firebase.initializeApp(
        name: appName,
        options: Platform.isIOS || Platform.isMacOS
            ? FirebaseOptions(
                appId: '1:57618466235:ios:caa132ea892737c070f09e',
                databaseURL: 'https://supchat-65619.firebaseio.com',
                storageBucket: 'supchat-65619.appspot.com',
                apiKey: 'AIzaSyCZFBWLQWkuc6TYW7gYXpAUJw7DFp-j6_4',
                messagingSenderId: "57618466235",
                authDomain: "supchat-65619.firebaseapp.com",
                measurementId: "G-0C9N7Q4NF5",
                projectId: 'supchat-65619',
              )
            : FirebaseOptions(
                appId: '1:57618466235:android:b405fd5b2af8bfc070f09e',
                databaseURL: 'https://supchat-65619.firebaseio.com',
                storageBucket: 'supchat-65619.appspot.com',
                messagingSenderId: "57618466235",
                measurementId: "G-0C9N7Q4NF5",
                authDomain: "supchat-65619.firebaseapp.com",
                apiKey: 'AIzaSyCYNryGZb9tcU742WXTh4zZjzAUm0TVsR8',
                projectId: 'supchat-65619',
              ),
      );
    } on FirebaseException catch (e) {
      if (e.code == 'duplicate-app') {
        Firebase.app(appName);
      } else {
        throw e;
      }
    } catch (e) {
      rethrow;
    }
  }
}

extension on DocumentSnapshot {
  DateTime dateTimeFromTimestamp() {
    if (data == null ||
        !data().containsKey('timestamp') ||
        data()['timestamp'] == null) return DateTime.now();
    return DateTime.parse(this['timestamp'].toDate().toString());
  }
}

extension on QuerySnapshot {
  List<Message> toMessages(String customerId) {
    return docs.map((document) {
      return Message(
          idFrom: document['idFrom'],
          content: document['content'],
          type: document['type'],
          time: document.dateTimeFromTimestamp(),
          isMy: document['idFrom'] == customerId,
          isLast: docs.first == document);
    }).toList();
  }
}
