
import 'package:firebase_database/firebase_database.dart';
import 'package:supchat/user/data/customer_user_repository.dart';

class OnlineStatusUpdator {
  
  final CustomerUserRepository _customerUserRepository;
  
  final _isOfflineForDatabase = {
    'state': 'offline',
    'last_changed': ServerValue.timestamp
  };

  final _isOnlineForDatabase = {
    'state': 'online',
    'last_changed': ServerValue.timestamp
  };

  OnlineStatusUpdator(this._customerUserRepository);

  void startStatusUpdating() async {

    final user = await _customerUserRepository.getUser();

    final String onlineStatusPath = "customers/${user.id}/online";
    print("////  ${onlineStatusPath}");
    var firebaseDatabase = FirebaseDatabase.instance;
    firebaseDatabase
        .reference()
        .child('.info/connected')
        .child('value')
        .onValue
        .map((event) {
      if (event.snapshot.value == false) {
        return event;
      }

      var onlineStatusRef = firebaseDatabase.reference().child(onlineStatusPath);
      onlineStatusRef
          .onDisconnect()
          .set(_isOfflineForDatabase)
          .then((value) => onlineStatusRef.set(_isOnlineForDatabase));
      return event;
    }).listen((event) {
      print("/// updated online status");
    });
  }

}