import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:supchat/analytics/data/analytics_repository.dart';
import 'package:supchat/crashlytics/crashlytics.dart';
import 'package:supchat_chat/chat/data/chat_repository.dart';
import '../../user/data/customer_user_repository.dart';
import '../../user/model/user.dart';
import 'bloc.dart';

class MainBloc extends Bloc<MainEvent, MainState> {
  final CustomerUserRepository _userRepository;
  final ChatRepository _chatRepository;
  final AnalyticsRepository _analyticsRepository;

  MainBloc(
    this._userRepository,
    this._chatRepository,
    this._analyticsRepository,
  ) : super(InitialState());

  @override
  Stream<MainState> mapEventToState(
    MainEvent event,
  ) async* {
    print(event);
    if (event is AppOpened) {
      await _chatRepository.initialize();
      await _analyticsRepository.logEvent("app_opened");

      initCrashlytics();
      final user = await _userRepository.getUser();
      print('existing user: ${user}');
      if (user == User.empty) {
        yield CustomerLoginState();
      } else {
        print("existing user is not empty");
        await FirebaseCrashlytics.instance.setUserIdentifier(user.id);
        await _analyticsRepository.setUserId(user.id);
        yield CustomerMainState(user.id);
      }
    }
  }
}
