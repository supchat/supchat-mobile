import 'package:meta/meta.dart';

@immutable
abstract class MainEvent {}

class AppOpened extends MainEvent {
  final String customerId;

  AppOpened(this.customerId);

}
