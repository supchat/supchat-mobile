
import 'package:meta/meta.dart';

@immutable
abstract class MainState {}

class InitialState extends MainState {}

class CustomerLoginState extends MainState {}

class CustomerMainState extends MainState {
  final String userId;

  CustomerMainState(this.userId);
}
