import 'package:flutter/material.dart';
import 'package:supchat/analytics/data/analytics_repository.dart';
import 'package:supchat/chats/chats_screen.dart';
import 'package:supchat_chat/chat/data/chat_repository.dart';
import 'package:supchat_chat/chat/data/user_repository.dart';
import '../landing/landing_screen.dart';
import '../sl/getIt.dart';
import 'bloc/bloc.dart';

import '../const.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class CustomerApp extends StatelessWidget {
  final String customerId;

  CustomerApp({this.customerId});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      builder: (context, child) {
        return ScrollConfiguration(
          behavior: SupChatScrollBehavior(),
          child: child,
        );
      },
      title: 'SupChat Demo',
      theme: ThemeData(
        primaryColor: themeColor,
      ),
      home: MultiBlocProvider(providers: [
        BlocProvider<MainBloc>(
          create: (context) => MainBloc(getIt<UserRepository>(),
              getIt<ChatRepository>(), getIt<AnalyticsRepository>())
            ..add(AppOpened(this.customerId)),
        ),
        // BlocProvider<AuthBloc>(
        //   create: (context) => AuthBloc(app: fbApp, saveUser: _saveUser),
        // )
      ], child: Scaffold(body: _buildInitialScreen())),
      debugShowCheckedModeBanner: false,
    );
  }

  Widget _buildInitialScreen() {
    return MultiBlocListener(
      listeners: [
        BlocListener<MainBloc, MainState>(listener: (context, state) {
          if (state is CustomerLoginState) {
            navigateToLogin(context);
          } else if (state is CustomerMainState) {
            navigateToChats(context, state.userId);
          }
        }),
      ],
      child: BlocBuilder<MainBloc, MainState>(builder: (_, state) {
        return Stack(
          children: <Widget>[if (state is InitialState) buildLoading()],
        );
      }),
    );

//    return SignUpScreen();
  }

  void navigateToLogin(BuildContext context) {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => LandingScreen()),
    );
  }

  void navigateToChats(BuildContext context, String userId) {
    Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) => ChatsScreen(
                currentUserId: userId,
              )),
    );
  }

  Widget buildLoading() {
    return Positioned(
        child: Container(
      child: Center(
        child: CircularProgressIndicator(
            valueColor: AlwaysStoppedAnimation<Color>(themeColor)),
      ),
      color: Colors.white.withOpacity(0.8),
    ));
  }
}

class SupChatScrollBehavior extends ScrollBehavior {
  @override
  ScrollPhysics getScrollPhysics(BuildContext context) {
    return BouncingScrollPhysics();
  }
}
