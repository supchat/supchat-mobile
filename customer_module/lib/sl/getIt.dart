import 'package:get_it/get_it.dart';
import 'package:logger/logger.dart';
import 'package:supchat/analytics/data/analytics_repository.dart';
import 'package:supchat/auth/data/auth_repository.dart';
import 'package:supchat/auth/data/social/apple.dart';
import 'package:supchat/auth/data/social/facebook.dart';
import 'package:supchat/auth/data/social/google.dart';
import 'package:supchat/billing/data/billing_repository.dart';
import 'package:supchat/online/OnlineStatusUpdator.dart';
import 'package:supchat/utils/connectivity_manager.dart';
import 'package:supchat_chat/chat_config.dart';
import 'package:supchat_chat/chat/data/chat_repository.dart';
import 'package:supchat_chat/chat/data/user_repository.dart';

import '../chat/data/customer_chat_repository.dart';
import '../chats/data/chats_repository.dart';
import '../chats/notifications/notificator.dart';
import '../user/data/customer_user_repository.dart';

final getIt = GetIt.instance;

initDependencies(ChatConfig config) {
  //repositories
  getIt.registerLazySingleton<AuthRepository>(
      () => AuthRepository(getIt<CustomerUserRepository>()));
  getIt.registerLazySingleton<GoogleRepository>(
      () => GoogleRepository(getIt<CustomerUserRepository>()));
  getIt.registerLazySingleton<FacebookRepository>(
      () => FacebookRepository(getIt<CustomerUserRepository>()));
  getIt.registerLazySingleton<AppleRepository>(
      () => AppleRepository(getIt<CustomerUserRepository>()));
  getIt.registerLazySingleton<BillingRepository>(
      () => BillingRepository(getIt<CustomerUserRepository>()));
  getIt.registerLazySingleton<ChatRepository>(() => CustomerChatRepository(
      getIt<ChatConfig>(),
      getIt<Logger>(),
      getIt<CustomerUserRepository>(),
      getIt<AnalyticsRepository>()));
  getIt.registerSingleton<ChatConfig>(config);
  getIt.registerLazySingleton<UserRepository>(
      () => getIt<CustomerUserRepository>());
  getIt.registerLazySingleton<CustomerUserRepository>(() =>
      CustomerUserRepository(
          getIt<ChatConfig>(), getIt<ConnectivityManager>()));
  getIt.registerLazySingleton<ChatsRepository>(
      () => ChatsRepository(getIt<ChatConfig>(), getIt<Logger>()));
  getIt.registerLazySingleton<AnalyticsRepository>(() => AnalyticsRepository());

  //utils
  getIt.registerLazySingleton<Notificator>(
      () => Notificator(getIt<CustomerUserRepository>()));
  getIt.registerLazySingleton<OnlineStatusUpdator>(
      () => OnlineStatusUpdator(getIt<CustomerUserRepository>()));
  getIt.registerLazySingleton<Logger>(() => Logger());
  getIt.registerLazySingleton<ConnectivityManager>(() => ConnectivityManager());
}
