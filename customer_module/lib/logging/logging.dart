import 'package:bloc/bloc.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:logger/logger.dart';
import 'package:supchat/sl/getIt.dart';

initBlocLogging() {
  Bloc.observer = AppBlocObserver(getIt<Logger>());
}

class AppBlocObserver extends BlocObserver {

  final Logger _logger;

  AppBlocObserver(this._logger);

  @override
  void onCreate(Cubit cubit) {
    super.onCreate(cubit);
    _logger.d('onCreate -- cubit: ${cubit.runtimeType}');
  }

  @override
  void onChange(Cubit cubit, Change change) {
    super.onChange(cubit, change);
    final text = 'onChange -- cubit: ${cubit.runtimeType}, change: $change';
    FirebaseCrashlytics.instance.log('$text');
    _logger.d(text);
  }

  @override
  void onError(Cubit cubit, Object error, StackTrace stackTrace) {
    final text = 'onError -- cubit: ${cubit.runtimeType}, error: $error';
    _logger.d(text);
    FirebaseCrashlytics.instance.log('$text');
    super.onError(cubit, error, stackTrace);
  }

  @override
  void onClose(Cubit cubit) {
    super.onClose(cubit);
    final text = 'onClose -- cubit: ${cubit.runtimeType}';
    FirebaseCrashlytics.instance.log('$text');
    _logger.d(text);
  }
}