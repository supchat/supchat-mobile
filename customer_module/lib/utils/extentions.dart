import 'package:intl/intl.dart';

extension datetime on DateTime {
  String formatTime() {
    final dateFormat = DateFormat('HH:MM');
    return dateFormat.format(this);
  }
}
