class Chat {
  final String id;
  final String title;
  final String avatar;
  final String lastMessage;
  final DateTime timestamp;
  final int unreadMessagesCount;

  Chat(this.id, this.title, this.avatar, this.lastMessage, this.timestamp,
      this.unreadMessagesCount);
  
  bool hasUnreadMessages() {
    return unreadMessagesCount != null && unreadMessagesCount > 0;
  }
}
