import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:supchat/user/data/customer_user_repository.dart';

class Notificator {
  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging.instance;
  final CustomerUserRepository _customerUserRepository;

  Notificator(this._customerUserRepository);

  void registerNotifications(String customerId) async {
    _firebaseMessaging.requestPermission();

    final token = await _firebaseMessaging.getToken();

    final user = await _customerUserRepository.getUser();

    await _customerUserRepository.updatePushToken(token);

  }
}
