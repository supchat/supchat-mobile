import '../../chats/model/chat.dart';

abstract class ChatsState {
  const ChatsState();
}

class InitialChatsState extends ChatsState {}

class NoChatsState extends ChatsState {}

class ChatsLoadedState extends ChatsState {
  final List<Chat> chats;

  ChatsLoadedState(this.chats);
}
