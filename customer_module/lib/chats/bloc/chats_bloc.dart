import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:supchat/analytics/data/analytics_repository.dart';
import 'package:supchat/online/OnlineStatusUpdator.dart';
import 'package:supchat/user/data/customer_user_repository.dart';
import 'package:supchat_chat/chat_config.dart';

import '../../chat/data/customer_chat_repository.dart';
import '../../chats/data/chats_repository.dart';
import '../../chats/notifications/notificator.dart';
import 'bloc.dart';

class ChatsBloc extends Bloc<ChatsEvent, ChatsState> {
  final ChatsRepository _chatsRepository;
  final CustomerChatRepository _chatRepository;
  final CustomerUserRepository _customerUserRepository;
  final Notificator _notificator;
  final OnlineStatusUpdator _onlineStatusUpdator;
  final ChatConfig _config;
  final AnalyticsRepository _analyticsRepository;

  ChatsBloc(
    this._chatsRepository,
    this._chatRepository,
    this._customerUserRepository,
    this._notificator,
    this._config,
    this._onlineStatusUpdator,
    this._analyticsRepository,
  ) : super(InitialChatsState());

  List<StreamSubscription> _subscriptions = [];

  @override
  Stream<ChatsState> mapEventToState(
    ChatsEvent event,
  ) async* {
    if (event is ChatsOpenedEvent) {
      _analyticsRepository.logEvent("chats_opened");
      yield* _mapChatsOpenedToState(event);
    } else if (event is ChatsUpdateEvent) {
      yield* _mapChatsUpdateToState(event);
    }
  }

  Stream<ChatsState> _mapChatsOpenedToState(ChatsOpenedEvent event) async* {
    _subscriptions
        .add(_chatRepository.updateOnline(event.userId).listen((event) {}));
    _subscriptions.add(_chatsRepository.getChats(event.userId).listen((chats) {
      add(ChatsUpdateEvent(chats));
    }));
    if (_config.isCustomerApp) {
      _notificator.registerNotifications(event.userId);
      _onlineStatusUpdator.startStatusUpdating();
    }
  }

  @override
  Future<Function> close() {
    _subscriptions.forEach((element) => element.cancel());
    _subscriptions.clear();
    return super.close();
  }

  Stream<ChatsState> _mapChatsUpdateToState(ChatsUpdateEvent event) async* {
    final chats = event.chats;
    if (chats == null || chats.isEmpty) {
      yield NoChatsState();
    } else {
      yield ChatsLoadedState(chats);
    }
  }

  Future<String> get apiKey async {
    final user = await this._customerUserRepository.getUser();
    return user.id;
  }
}
