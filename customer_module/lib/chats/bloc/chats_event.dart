
import '../../chats/model/chat.dart';

abstract class ChatsEvent  {
  const ChatsEvent();
}


class ChatsOpenedEvent extends ChatsEvent {
  final String userId;

  ChatsOpenedEvent(this.userId);
}

class ChatsUpdateEvent extends ChatsEvent {
  final List<Chat> chats;

  ChatsUpdateEvent(this.chats);
}