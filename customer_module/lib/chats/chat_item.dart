import 'dart:core';
import 'dart:math';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:supchat/analytics/data/analytics_repository.dart';
import 'package:supchat/details/details_screen.dart';
import 'package:supchat/sl/getIt.dart';
import 'package:supchat_chat/chat_config.dart';
import 'package:supchat_chat/chat/chat_screen.dart';
import 'package:supchat_chat/chat/data/chat_repository.dart';
import 'package:supchat_chat/chat/data/user_repository.dart';

import '../utils/extentions.dart';
import '../details/details_screen.dart';
import '../sl/getIt.dart';
import 'model/chat.dart';

class ChatItem extends StatelessWidget {
  final Chat chat;
  final String currentUserId;

  ChatItem({@required this.chat, @required this.currentUserId});

  @override
  Widget build(BuildContext context) {
    return FlatButton(
      highlightColor: Color(0xffF5F5F7),
      padding: const EdgeInsets.all(12.0),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          _buildAvatar(chat),
          Flexible(
            child: Container(
              width: double.maxFinite,
              margin: const EdgeInsets.only(left: 12.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    '${chat.title ?? chat.id}',
                    overflow: TextOverflow.ellipsis,
                    style: GoogleFonts.montserrat(
                        textStyle: const TextStyle(
                            fontSize: 15.0,
                            fontWeight: FontWeight.w500,
                            color: Color(0xff000A1F))),
                  ),
                  const SizedBox(height: 4.0),
                  Text(
                    '${chat.lastMessage ?? ''}',
                    maxLines: 1,
                    overflow: TextOverflow.ellipsis,
                    style: GoogleFonts.montserrat(
                        textStyle: const TextStyle(
                            fontSize: 12.0,
                            fontWeight: FontWeight.w300,
                            color: const Color(0xff55658A))),
                  ),
                ],
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 16.0),
            child: Column(
              children: <Widget>[
                Text(
                  chat.timestamp?.formatTime() ?? '00:00',
                  style: GoogleFonts.montserrat(
                      textStyle: const TextStyle(
                    fontSize: 11.0,
                    color: const Color(0xff55658A),
                  )),
                ),
                const SizedBox(height: 2.0),
                (chat.hasUnreadMessages())
                    ? Container(
                        padding: const EdgeInsets.all(6.0),
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          color: const Color(0xffFF5F3C),
                        ),
                        child: Text(
                          '${chat.unreadMessagesCount}',
                          textAlign: TextAlign.center,
                          style: GoogleFonts.montserrat(
                              textStyle: const TextStyle(
                                  fontSize: 10,
                                  color: Colors.white,
                                  fontWeight: FontWeight.w500)),
                        ),
                      )
                    : const SizedBox(height: 24.0, width: 24.0)
              ],
            ),
          )
        ],
      ),
      onPressed: () {
        getIt<AnalyticsRepository>().logEvent("chat_opened");
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => ChatScreen(
                      chatId: chat.id,
                      userId: currentUserId,
                      userRepository: getIt<UserRepository>(),
                      chatRepository: getIt<ChatRepository>(),
                      chatConfig: getIt<ChatConfig>(),
                      onDetailsClick: (context) {
                        getIt<AnalyticsRepository>()
                            .logEvent("user_details_opened");
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => DetailsScreen(
                                      userId: chat.id,
                                      customerId: currentUserId,
                                    )));
                      },
                    )));
      },
      color: Colors.white,
    );
  }

  Widget _buildAvatar(Chat chat) {
    var title = chat.title ?? chat.id;

    return chat.avatar != null
        ? Image(
            image: NetworkImage(chat.avatar),
            width: 48.0,
            height: 48.0,
            fit: BoxFit.cover,
          )
        : Container(
            width: 48.0,
            height: 48.0,
            child: CircleAvatar(
              backgroundColor: color(title),
              child: Center(
                child: Text(
                  title[0].toUpperCase(),
                  style: GoogleFonts.montserrat(
                    textStyle: const TextStyle(
                      color: Colors.white,
                      fontSize: 24,
                    ),
                  ),
                ),
              ),
            ),
          );
  }

  Color color(String base) {
    final rest = ((base.hashCode % Colors.primaries.length));
    return Colors.primaries[rest];
  }
}
