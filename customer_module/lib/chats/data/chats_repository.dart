import 'dart:async';

import 'package:logger/logger.dart';
import 'package:supchat_chat/chat_config.dart';

import '../../chats/model/chat.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class ChatsRepository {
  final ChatConfig _config;

  ChatsRepository(this._config, this._logger);

  final FirebaseFirestore _firestore = FirebaseFirestore.instance;

  final Logger _logger;

  Stream<List<Chat>> getChats(String customerId) async* {
    final chatsStream =
        _firestore
            .collection('customers/$customerId/chats')
            .where('justCreated', isEqualTo: false)
            .snapshots();
    yield* chatsStream.map((chats) {
      return chats.docs.map((chat) {
        return chat.toChat();
      }).toList()
        ..sort((chat1, chat2) => _compare(chat1, chat2));
    });
  }

  int _compare(Chat chat1, Chat chat2) {
    DateTime dateTime1 = chat1.timestamp;
    DateTime dateTime2 = chat2.timestamp;

    if (chat1.hasUnreadMessages() && !chat2.hasUnreadMessages()) return -1;
    if (!chat1.hasUnreadMessages() && chat2.hasUnreadMessages()) return 1;

    if (dateTime1 != null && dateTime2 == null) return -1;
    if (dateTime1 == null && dateTime2 == null) return 0;
    if (dateTime1 == null && dateTime2 != null) return 1;
    return (dateTime2.compareTo(dateTime1));
  }
}

extension on QueryDocumentSnapshot {
  Chat toChat() {
    final id = this.id;
    final avatar = this.data()['avatar'];
    final title = this.data()['nickname'] ?? id;
    final lastMessageData = this.data()['lastMessage'];
    final content =
        lastMessageData['type'] == 0 ? lastMessageData['content'] : '📎 image';
    //timestamp is null when local changes trigger listener
    final timestamp = DateTime.tryParse(
            lastMessageData['timestamp']?.toDate()?.toString() ?? "") ??
        DateTime.now();
    final unreadCount = this['unreadMessagesCount'];
    return Chat(id, title, avatar, content, timestamp, unreadCount);
  }
}
