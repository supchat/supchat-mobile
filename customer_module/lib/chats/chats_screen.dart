import 'dart:async';
import 'dart:core';
import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:supchat/analytics/data/analytics_repository.dart';
import 'package:supchat/online/OnlineStatusUpdator.dart';
import 'package:supchat/profile/settings_screen.dart';
import 'package:supchat/widgets/atoms/content_container.dart';
import 'package:supchat/widgets/yes_no_dialog.dart';
import 'package:supchat_chat/chat_config.dart';
import 'package:supchat_chat/chat/data/chat_repository.dart';
import 'package:supchat_chat/chat/data/user_repository.dart';
import 'bloc/bloc.dart';
import 'chat_item.dart';
import 'data/chats_repository.dart';

import '../sl/getIt.dart';

import '../const.dart';

import 'package:flutter_bloc/flutter_bloc.dart';

import 'notifications/notificator.dart';

class ChatsScreen extends StatefulWidget {
  final String currentUserId;

  ChatsScreen({Key key, @required this.currentUserId}) : super(key: key);

  @override
  State createState() => ChatsScreenState(currentUserId: currentUserId);
}

class ChatsScreenState extends State<ChatsScreen> {
  ChatsScreenState({Key key, @required this.currentUserId});

  final TextEditingController searchController =
      TextEditingController(text: '');

  final String currentUserId;
  bool isLoading = false;
  bool isSearchOpen = false;

  @override
  void initState() {
    super.initState();
    //_initImages();
  }

  Future<bool> onBackPress() {
    _openExitDialog();
    return Future.value(false);
  }

  Future<Null> _openExitDialog() async {
    final result = await showDialog<Result>(
      context: context,
      builder: (BuildContext context) {
        return YesNoDialog(
          message: 'Are you sure you want to exit the app?',
          yesButtonText: 'Exit',
          noButtonText: 'Cancel',
        );
      },
    );
    if (result == Result.yes) {
      exit(0);
    }
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider<ChatsBloc>(
      create: (BuildContext context) => ChatsBloc(
          getIt<ChatsRepository>(),
          getIt<ChatRepository>(),
          getIt<UserRepository>(),
          getIt<Notificator>(),
          getIt<ChatConfig>(),
          getIt<OnlineStatusUpdator>(),
          getIt<AnalyticsRepository>())
        ..add(ChatsOpenedEvent(currentUserId)),
      child: Scaffold(
        backgroundColor: Colors.white,
        body: WillPopScope(
          child: BlocBuilder<ChatsBloc, ChatsState>(
            builder: (context, state) => Stack(
              children: <Widget>[
                _buildTopBar(),
                Container(
                  color: Colors.transparent,
                  margin: const EdgeInsets.only(top: 80.0),
                  child: ContentContainer(
                    child: Column(
                      children: <Widget>[
                        _buildTitleContent(),
                        _buildContent(state),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
          onWillPop: onBackPress,
        ),
      ),
    );
  }

  Widget _buildTitleContent() {
    if (isSearchOpen) {
      return _buildSearchField();
    } else {
      return _buildTopTitle();
    }
  }

  Widget _buildContent(ChatsState state) {
    return Expanded(
        child: state.isNotLoadedYet()
            ? _buildProgress()
            : state.emptyChats()
                ? _buildEmptyChats()
                : _buildChatsList(state as ChatsLoadedState));
  }

  Widget _buildEmptyChats() {
    return Center(child: Text('No chats yet', style: GoogleFonts.montserrat()));
  }

  Widget _buildChatsList(ChatsLoadedState state) {
    var query = searchController.text;
    var filteredChats = state.chats
        .where((i) =>
            query.isEmpty ||
            i.title.toLowerCase().contains(query.toLowerCase()))
        .toList();

    return ListView.builder(
      padding: const EdgeInsets.only(top: 24.0, bottom: 24.0),
      itemBuilder: (context, index) =>
          ChatItem(chat: filteredChats[index], currentUserId: currentUserId),
      itemCount: filteredChats.length,
    );
  }

  Center _buildProgress() {
    return Center(
      child: CircularProgressIndicator(
        valueColor: AlwaysStoppedAnimation<Color>(themeColor),
      ),
    );
  }

  SafeArea _buildTopBar() {
    return SafeArea(
      top: false,
      child: Container(
        width: double.maxFinite,
        color: const Color(0xffFF5F3B),
        child: SvgPicture.asset(
          'assets/chats_bg.svg',
          fit: BoxFit.fill,
        ),
      ),
    );
  }

  Widget _buildTopTitle() {
    return Padding(
        padding: const EdgeInsets.only(top: 12.0),
        child: Material(
          color: Colors.white,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              IconButton(
                icon: const Icon(Icons.search),
                tooltip: 'Search',
                color: const Color(0xff0F2253),
                onPressed: () {
                  setState(() {
                    isSearchOpen = true;
                  });
                },
              ),
              Text(
                'Recent messages',
                style: GoogleFonts.montserrat(
                  textStyle: TextStyle(
                      fontSize: 15.0,
                      color: const Color(0xff0F2253),
                      fontWeight: FontWeight.w500),
                ),
              ),
              Builder(
                builder: (context) => IconButton(
                  icon: const Icon(Icons.account_circle_outlined),
                  tooltip: 'User profile',
                  color: const Color(0xff0F2253),
                  onPressed: () async {
                    final apiKey =
                        await BlocProvider.of<ChatsBloc>(context).apiKey;
                    return Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => SettingsScreen(apiKey: apiKey),
                        ));
                  },
                ),
              ),
            ],
          ),
        ));
  }

  Widget _buildSearchField() {
    return Column(
      children: [
        const SizedBox(height: 12.0),
        Container(
          decoration: BoxDecoration(
            boxShadow: <BoxShadow>[
              BoxShadow(
                color: Colors.red.withOpacity(0.3),
                blurRadius: 3.0,
              ),
            ],
          ),
          child: Material(
            color: Colors.white,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                IconButton(
                  icon: const Icon(Icons.search, color: Colors.black38),
                  tooltip: 'Search',
                ),
                Expanded(
                  child: TextField(
                    controller: searchController,
                    autofocus: true,
                    decoration: InputDecoration(
                      border: InputBorder.none,
                      hintText: 'Search',
                    ),
                    onChanged: (text) {
                      setState(() {});
                    },
                  ),
                ),
                IconButton(
                  icon: const Icon(Icons.close),
                  tooltip: 'Close search',
                  onPressed: () {
                    searchController.text = "";
                    setState(() {
                      isSearchOpen = false;
                    });
                  },
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }

  @override
  void dispose() {
    searchController.dispose();
    super.dispose();
  }
}

class Choice {
  const Choice({this.title, this.icon});

  final String title;
  final IconData icon;
}

extension on ChatsState {
  bool isNotLoadedYet() {
    return this is InitialChatsState;
  }

  bool emptyChats() {
    return this is NoChatsState;
  }
}
